﻿using Auth.Interfaces.Dto;

namespace Auth.Interfaces;

public interface IJwtUtils
{
    public string GenerateJwtToken(AuthUserDto user);
    public int? ValidateJwtToken(string token);
    public AuthRefreshTokenDto GenerateRefreshToken(string ipAddress);
}