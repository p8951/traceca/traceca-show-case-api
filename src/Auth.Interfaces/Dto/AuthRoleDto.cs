﻿using Utils.Enums;

namespace Auth.Interfaces.Dto;

public class AuthRoleDto
{
    public RoleEnum Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
}