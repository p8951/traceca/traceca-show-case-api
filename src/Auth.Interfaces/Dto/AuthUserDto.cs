﻿namespace Auth.Interfaces.Dto;

public class AuthUserDto
{
    public int Id { get; set; }
    public List<AuthRoleDto> Roles { get; set; }
}