﻿namespace Auth.Interfaces;

public class JwtAppSettings
{
    public string Secret { get; set; }
    public int RefreshTokenTTL { get; set; }
    public string Salt { get; set; }
    public string Issuer { get; set; }
    public string Audience { get; set; }
}