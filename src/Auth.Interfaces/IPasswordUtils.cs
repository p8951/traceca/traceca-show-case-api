﻿namespace Auth.Interfaces;

public interface IPasswordUtils
{
    public bool Verify(string text, string textHash);
}