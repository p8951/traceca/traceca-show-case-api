using Auth.Interfaces;
using Auth.Jwt;
using DataAccess.Interfaces;
using DataAccess.PostgreSql;
using Microsoft.EntityFrameworkCore;
using Web.UseCases;
using WebApi.Middlewares;
using WebApi.Services;
using WebApp.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
{
    var services = builder.Services;
    var configuration = builder.Configuration;
    services.Configure<JwtAppSettings>(configuration.GetSection("JWT"));
    
    services.AddControllers();
    services.AddHttpContextAccessor();
    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();
    services.AddCors();
    
    services.RegisterDb(configuration);
    services.RegisterAuth(configuration);
    
    services.AddScoped<IHttpContextService, HttpContextService>();
    services.RegisterWebUseCases();
}

var app = builder.Build();

// Configure the HTTP request pipeline.
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }
    else
    {
        
    }
    using (var scope = app.Services.CreateScope())
    {
        var dataContext = scope.ServiceProvider.GetRequiredService<IDbContext>();
        dataContext.Database.Migrate();
    }
    app.UseCors(x => x
        .SetIsOriginAllowed(origin => true)
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials());
    app.UseHttpsRedirection();
    app.UseAuthentication();
    app.UseAuthorization();
    
    app.MapControllers();
    // global error handler
    app.UseMiddleware<ErrorHandlerMiddleware>();
}
app.Run();