﻿using System.Security.Claims;
using WebApp.Interfaces;

namespace WebApi.Services
{
    public class HttpContextService : IHttpContextService
    {
        public int UserId { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public HttpContextService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            var userIdStr = httpContextAccessor.HttpContext?.User?.FindFirstValue("id");
            UserId =  userIdStr != null ? Int32.Parse(userIdStr) : 0;
        }
        
        
    }
}