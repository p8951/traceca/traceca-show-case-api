﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Interfaces;

namespace WebApi.Controllers.Common;

[Route("api/[controller]")]
[ApiController]
[Authorize]
public abstract class BaseController: ControllerBase
{
    protected readonly IHttpContextService HttpContextService;
    protected ISender Sender { get; }


    public BaseController(ISender sender, IHttpContextService httpContextService)
    {
        HttpContextService = httpContextService;
        Sender = sender;
    }
    protected string IpAddress()
    {
        // get source ip address for the current request
        if (Request.Headers.ContainsKey("X-Forwarded-For"))
            return Request.Headers["X-Forwarded-For"];
        else
            return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
    }
}