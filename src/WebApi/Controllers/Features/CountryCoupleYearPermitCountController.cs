﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Commands;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Queries;
using Web.UseCases.Features.Permit.Commands;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class CountryCoupleYearPermitCountController: BaseController
{
    public CountryCoupleYearPermitCountController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]CountryCoupleYearPermitCountCreateDto request)
    {
        var response = await Sender.Send(new CountryCoupleYearPermitCountCreateCommand {Dto = request});
        var countryCoupleYearPermitCount = await Sender.Send(new CountryCoupleYearPermitCountGetByIdQuery() {Id = response});
        await Sender.Send(new PermitGenerateCommand() {CountryCoupleYearPermitCount = countryCoupleYearPermitCount, startAt = 1});
        return Ok(response);
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] CountryCoupleYearPermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CountryCoupleYearPermitCountPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] CountryCoupleYearPermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CountryCoupleYearPermitCountListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("Current")]
    public async Task<IActionResult> GetListCurrent()
    {
        var currentUser = await Sender.Send(new UserByIdQuery {Id = HttpContextService.UserId}); 
        var filter = new CountryCoupleYearPermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            FirstCountryId = currentUser.CountryId
        };
        var response = await Sender.Send(new CountryCoupleYearPermitCountListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpPut("{id:int}/AddPermitCount")]
    public async Task<IActionResult> GetList(int id, [FromBody] CountryCoupleYearPermitCountAddCountDto request)
    {
        await Sender.Send(new CountryCoupleYearPermitCountUpdateCommand() {Id = id, Dto = request});
        var countryCoupleYearPermitCount = await Sender.Send(new CountryCoupleYearPermitCountGetByIdQuery() {Id = id});
        var newStartNumber = countryCoupleYearPermitCount.CountPermit - request.CountPermit;
        await Sender.Send(new PermitGenerateCommand() {CountryCoupleYearPermitCount = countryCoupleYearPermitCount, startAt = newStartNumber + 1});
        return Ok();
    }
}