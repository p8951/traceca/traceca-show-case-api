﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.Driver.Commands;
using Web.UseCases.Features.Driver.Dto;
using Web.UseCases.Features.Driver.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class DriverController: BaseController
{
    public DriverController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] DriverFilterDto filter)
    {
        var user = await Sender.Send(new UserByIdQuery { Id = HttpContextService.UserId});
        filter.CarrierId = user.CarrierId;
        var response = await Sender.Send(new DriverPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] DriverFilterDto filter)
    {
        var user = await Sender.Send(new UserByIdQuery { Id = HttpContextService.UserId});
        filter.CarrierId = user.CarrierId;
        var response = await Sender.Send(new DriverListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] DriverCreateDto request)
    {
        var response = await Sender.Send(new DriverCreateCommand() {Dto = request});
        return Ok(response);
    }
}