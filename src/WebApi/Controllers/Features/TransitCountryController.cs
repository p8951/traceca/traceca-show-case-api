﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.TransitCountry.Commands;
using Web.UseCases.Features.TransitCountry.Dto;
using Web.UseCases.Features.TransitCountry.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class TransitCountryController: BaseController
{
    public TransitCountryController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]TransitCountryCreateDto request)
    {
        var response = await Sender.Send(new TransitCountryCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] TransitCountryFilterDto filter)
    {
        var response = await Sender.Send(new TransitCountryListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpDelete("{id:int}")]
    public async Task<IActionResult> Remove(int id)
    {
        var response = await Sender.Send(new TransitCountryDeleteCommand() {Id = id});
        return Ok(response);
    }
    
    [HttpDelete("CountryCouple/{id:int}")]
    public async Task<IActionResult> RemoveByCountryCouple(int id)
    {
        var response = await Sender.Send(new TransitCountryCountryCoupleDeleteCommand() {CountryCoupleYearPermitCountId = id});
        return Ok(response);
    }
}