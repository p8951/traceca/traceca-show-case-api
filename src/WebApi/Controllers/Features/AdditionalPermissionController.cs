﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.AdditionalPermission.Commands;
using Web.UseCases.Features.AdditionalPermission.Dto;
using Web.UseCases.Features.AdditionalPermission.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class AdditionalPermissionController: BaseController
{
    public AdditionalPermissionController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]AdditionalPermissionCreateDto request)
    {
        var response = await Sender.Send(new AdditionalPermissionCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] AdditionalPermissionFilterDto filter)
    {
        var response = await Sender.Send(new AdditionalPermissionListQuery() {Filter = filter});
        return Ok(response);
    }
}