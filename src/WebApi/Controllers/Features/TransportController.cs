﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.Transport.Commands;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.Transport.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class TransportController: BaseController
{
    public TransportController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] TransportFilterDto filter)
    {
        var response = await Sender.Send(new TransportPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody] TransportCreateDto request)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId});
        request.CarrierId = currentUser.CarrierId.Value;
        var response = await Sender.Send(new TransportCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet("PagedCurrent")]
    public async Task<IActionResult> GetPagedCurrentList([FromQuery] TransportFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId});

        filter.CarrierId = currentUser.CarrierId;

        var response = await Sender.Send(new TransportPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] TransportFilterDto filter)
    {
        var response = await Sender.Send(new TransportListQuery() {Filter = filter});
        return Ok(response);
    }
}