﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.Common.Paged;
using Web.UseCases.Features.CountryCouplePermitCount.Commands;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using Web.UseCases.Features.CountryCouplePermitCount.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class CountryCouplePermitCountController: BaseController
{
    public CountryCouplePermitCountController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]CountryCouplePermitCountCreateDto request)
    {
        var response = await Sender.Send(new CountryCouplePermitCountCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] CountryCouplePermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CountryCouplePermitCountPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("PagedCurrentB")]
    public async Task<IActionResult> GetPagedCurrentBList([FromQuery] CountryCouplePermitCountBaseFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId}); 
        var newfilter = new CountryCouplePermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            FirstCountryId = currentUser.CountryId,
            PageNumber = filter.PageNumber,
            PageSize = filter.PageSize
        };
        var response = await Sender.Send(new CountryCouplePermitCountPagedListQuery() {Filter = newfilter});
        return Ok(response);
    }

    [HttpGet("PagedCurrentA")]
    public async Task<IActionResult> GetPagedCurrentAList([FromQuery] CountryCouplePermitCountBaseFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId}); 
        var newfilter = new CountryCouplePermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            SecondCountryId = currentUser.CountryId,
            PageNumber = filter.PageNumber,
            PageSize = filter.PageSize
        };
        var response = await Sender.Send(new CountryCouplePermitCountPagedListQuery() {Filter = newfilter});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] CountryCouplePermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CountryCouplePermitCountListQuery() );
        return Ok(response);
    }
    
    [HttpPut("{id:int}/AddPermitCount")]
    public async Task<IActionResult> GetList(int id, [FromBody] CountryCouplePermitCountAddCountDto request)
    {
        await Sender.Send(new CountryCouplePermitCountUpdateCommand() {Id = id, Dto = request});
        return Ok();
    }
    
    [HttpGet("Current")]
    public async Task<IActionResult> GetListCurrent()
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId}); 
        var filter = new CountryCouplePermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            SecondCountryId = currentUser.CountryId
        };
        var response = await Sender.Send(new CountryCouplePermitCountListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("CurrentB")]
    public async Task<IActionResult> GetListCurrentB()
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId}); 
        var filter = new CountryCouplePermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            FirstCountryId = currentUser.CountryId
        };
        var response = await Sender.Send(new CountryCouplePermitCountListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("CurrentA")]
    public async Task<IActionResult> GetListCurrentA()
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId}); 
        var filter = new CountryCouplePermitCountFilterDto
        {
            Year = (short) DateTime.UtcNow.Year,
            SecondCountryId = currentUser.CountryId
        };
        var response = await Sender.Send(new CountryCouplePermitCountListQuery() {Filter = filter});
        return Ok(response);
    }
}