﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.CarrierPermitCount.Commands;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.CarrierPermitCount.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class CarrierPermitCountController: BaseController
{
    public CarrierPermitCountController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]CarrierPermitCountCreateDto request)
    {
        var response = await Sender.Send(new CarrierPermitCountCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] CarrierPermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CarrierPermitCountPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("PagedCurrent")]
    public async Task<IActionResult> GetPagedCurrentList([FromQuery] CarrierPermitCountFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId});

        filter.SecondCountryId = currentUser.CountryId;
        
        var response = await Sender.Send(new CarrierPermitCountPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] CarrierPermitCountFilterDto filter)
    {
        var response = await Sender.Send(new CarrierPermitCountListQuery() );
        return Ok(response);
    }
    
    [HttpPut("{id:int}/AddPermitCount")]
    public async Task<IActionResult> GetList(int id, [FromBody] CarrierPermitCountAddCountDto request)
    {
        await Sender.Send(new CarrierPermitCountUpdateCommand() {Id = id, Dto = request});
        return Ok();
    }
}