﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.UserRegistration.Commands;
using Web.UseCases.Features.UserRegistration.Dto;
using Web.UseCases.Features.UserRegistration.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class UserRegistrationController: BaseController
{
    public UserRegistrationController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]UserRegistrationCreateDto request)
    {
        var response = await Sender.Send(new UserRegistrationCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet]
    public async Task<IActionResult> GetPagedList([FromQuery] UserRegistrationFilterDto filter)
    {
        var response = await Sender.Send(new UserRegistrationPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpPut("{id:guid}/activate")]
    public async Task<IActionResult> Activate(Guid id)
    {
        await Sender.Send(new UserRegistrationActivatedCommand() {Id = id});
        return Ok();
    }
}