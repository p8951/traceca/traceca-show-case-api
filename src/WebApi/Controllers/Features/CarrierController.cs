﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.Carrier.Dto;
using Web.UseCases.Features.Carrier.Queries;
using Web.UseCases.Features.Dictionary.Country.Dto;
using Web.UseCases.Features.Dictionary.Country.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class CarrierController: BaseController
{
    public CarrierController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] CarrierFilterDto filter)
    {
        var response = await Sender.Send(new CarrierPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("PagedCurrent")]
    public async Task<IActionResult> GetPagedCurrentList([FromQuery] CarrierFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId});
        filter.CountryId = currentUser.CountryId;
        var response = await Sender.Send(new CarrierPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] CarrierFilterDto filter)
    {
        var response = await Sender.Send(new CarrierListQuery() {Filter = filter});
        return Ok(response);
    }

    
}