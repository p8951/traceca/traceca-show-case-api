﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.TransportPermit.Commands;
using Web.UseCases.Features.TransportPermit.Dto;
using Web.UseCases.Features.TransportPermit.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Features;

public class TransportPermitController: BaseController
{
    public TransportPermitController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]TransportPermitCreateDto request)
    {
        var response = await Sender.Send(new TransportPermitCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] TransportPermitFilterDto filter)
    {
        var response = await Sender.Send(new TransportPermitPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("PagedCurrent")]
    public async Task<IActionResult> GetPagedCurrentList([FromQuery] TransportPermitFilterDto filter)
    {
        var currentUser = await Sender.Send(new UserByIdQuery() {Id = HttpContextService.UserId});
        filter.SecondCountryId = currentUser.CountryId;
        var response = await Sender.Send(new TransportPermitPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] TransportPermitFilterDto filter)
    {
        var response = await Sender.Send(new TransportPermitListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("PagedHistory")]
    public async Task<IActionResult> GetPagedHistoryList([FromQuery] TransportPermitHistoryFilterDto filter)
    {
        var response = await Sender.Send(new TransportPermitHistoryPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    
    [HttpGet("CurrentUserPermitList")]
    public async Task<IActionResult> CurrentUserPermitList([FromQuery] TransportPermitFilterDto filter)
    {
        filter.UserId = HttpContextService.UserId;
        var response = await Sender.Send(new TransportPermitPagedListQuery() {Filter = filter});
        return Ok(response);
    }
    
    [HttpGet("ByPermitCode/{permitCode}")]
    public async Task<IActionResult> ByPermitCode(string permitCode)
    {
        var response = await Sender.Send(new TransportPermitByPermitCodeQuery() {PermitCode = permitCode});
        return Ok(response);
    }
}