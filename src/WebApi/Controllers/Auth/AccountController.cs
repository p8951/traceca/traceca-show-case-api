﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Commands;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Auth.User.Queries;
using Web.UseCases.Features.UserRegistration.Commands;
using Web.UseCases.Features.UserRegistration.Dto;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Auth;

public class AccountController : BaseController
{
    private readonly IHttpContextService _httpContextService;

    public AccountController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
        _httpContextService = httpContextService;
    }

    [AllowAnonymous]
    [HttpPost("authenticate")]
    public async Task<IActionResult> Auth([FromBody] AuthenticateRequestDto request)
    {
        var response = await Sender.Send(new UserAuthenticateCommand {Dto = request, IpAddress = IpAddress()});
        SetTokenCookie(response.RefreshToken);
        return Ok(response);
    }

    [AllowAnonymous]
    [HttpPost("refresh-token")]
    public async Task<IActionResult> RefreshToken()
    {
        var refreshToken = Request.Cookies["refreshToken"];
        var response = await Sender.Send(new UserRefreshTokenCommand
            {RefreshToken = refreshToken, IpAddress = IpAddress()});
        SetTokenCookie(response.RefreshToken);
        return Ok(response);
    }

    [HttpGet("user-info")]
    public async Task<IActionResult> UserInfo()
    {
        var response = await Sender.Send(new UserByIdQuery {Id = _httpContextService.UserId});
        return Ok(response);
    }

    private void SetTokenCookie(string token)
    {
        // append cookie with refresh token to the http response
        var cookieOptions = new CookieOptions
        {
            HttpOnly = true,
            Expires = DateTime.UtcNow.AddDays(7)
        };
        Response.Cookies.Append("refreshToken", token, cookieOptions);
    }
}