﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Auth.User.Commands;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Auth.User.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Auth;

public class UserController: BaseController
{
    public UserController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]UserCreateDto request)
    {
        var response = await Sender.Send(new UserCreateCommand {Dto = request});
        return Ok(response);
    }
    
    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetById(int id)
    {
        var response = await Sender.Send(new UserByIdQuery {Id = id});
        return Ok(response);
    }

    
}