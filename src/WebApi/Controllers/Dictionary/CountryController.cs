﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.UseCases.Features.Dictionary.Country.Commands;
using Web.UseCases.Features.Dictionary.Country.Dto;
using Web.UseCases.Features.Dictionary.Country.Queries;
using WebApi.Controllers.Common;
using WebApp.Interfaces;

namespace WebApi.Controllers.Dictionary;

public class CountryController: BaseController
{
    public CountryController(ISender sender, IHttpContextService httpContextService) : base(sender, httpContextService)
    {
    }
    
    [HttpPost]
    public async Task<IActionResult> Create([FromBody]CountryCreateDto request)
    {
        var response = await Sender.Send(new CountryCreateCommand() {Dto = request});
        return Ok(response);
    }
    
    [AllowAnonymous]
    [HttpGet("Paged")]
    public async Task<IActionResult> GetPagedList([FromQuery] CountryFilterDto filter)
    {
        var response = await Sender.Send(new CountryPagedListQuery {Filter = filter});
        return Ok(response);
    }
    
    [AllowAnonymous]
    [HttpGet]
    public async Task<IActionResult> GetList([FromQuery] CountryFilterDto filter)
    {
        var response = await Sender.Send(new CountryListQuery() {Filter = filter});
        return Ok(response);
    }
}