﻿using Entities;
using Entities.Auth;
using Entities.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace DataAccess.Interfaces;

public interface IDbContext
{
    Task<int> SaveChangesAsync(CancellationToken token = default);
    public DatabaseFacade Database { get; }
    DbSet<TEntity> Set<TEntity>() where TEntity : class;

    #region Auth

    public DbSet<Role> Roles { get; set; }
    public DbSet<User> Users { get; set; }

    #endregion

    #region Dictionary
    DbSet<Country> Countries { get; set; }

    #endregion
    
    DbSet<UserRegistration> UserRegistrations { get; set; }
    DbSet<Carrier> Carriers { get; set; }
    DbSet<CarrierPermitCount> CarrierPermitCounts { get; set; }
    DbSet<Transport> Transports { get; set; }
    DbSet<TransportPermit> TransportPermits { get; set; }
    DbSet<CountryCoupleYearPermitCount> CountryCoupleYearPermitCounts { get; set; }
    DbSet<CountryCouplePermitCount> CountryCouplePermitCounts { get; set; }
    DbSet<Permit> Permits { get; set; }
    DbSet<TransitCountry> TransitCountries { get; set; }
    DbSet<AdditionalPermission> AdditionalPermissions { get; set; }
    DbSet<DriverCarrier> DriverCarriers { get; set; }
    
}