﻿using Auth.Interfaces.Dto;
using AutoMapper;
using Entities;
using Entities.Auth;
using Entities.Dictionary;
using Web.UseCases.Features.AdditionalPermission.Dto;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Carrier.Dto;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using Web.UseCases.Features.Dictionary.Country.Dto;
using Web.UseCases.Features.Driver.Dto;
using Web.UseCases.Features.Permit.Dto;
using Web.UseCases.Features.TransitCountry.Dto;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using Web.UseCases.Features.UserRegistration.Dto;

namespace Web.UseCases;

public class WebUseCasesMapperProfile: Profile
{
    public WebUseCasesMapperProfile()
    {
        AuthMapped();
        DictionaryMapped();
        FeaturesMapped();
    }

    private void AuthMapped()
    {
        CreateMap<Role, AuthRoleDto>();
        CreateMap<Role, RoleDto>();
        CreateMap<User, AuthUserDto>();
        CreateMap<User, UserDto>();
        CreateMap<AuthRefreshTokenDto, RefreshToken>().ReverseMap();
        CreateMap<AuthRefreshTokenDto, RefreshToken>().ReverseMap();
        CreateMap<UserRegistration, User>().ReverseMap();
    }
    
    private void DictionaryMapped()
    {
        CreateMap<Country, CountryDto>();
    }
    
    private void FeaturesMapped()
    {
        #region UserRegistration
        CreateMap<UserRegistrationCreateDto, UserRegistration>();
        CreateMap<UserRegistration, UserRegistrationDto>();
        CreateMap<UserRegistration, User>();
        #endregion

        #region Permit
        CreateMap<CountryCoupleYearPermitCount, CountryCoupleYearPermitCountDto>();
        CreateMap<CountryCoupleYearPermitCountCreateDto, CountryCoupleYearPermitCount>();
        CreateMap<CountryCouplePermitCountCreateDto, CountryCouplePermitCount>();
        CreateMap<CountryCouplePermitCount, CountryCouplePermitCountDto>();
        
        
        CreateMap<Carrier, CarrierDto>();
        CreateMap<Transport, TransportDto>();
        
        CreateMap<CarrierPermitCount, CarrierPermitCountDto>();
        CreateMap<CarrierPermitCountCreateDto, CarrierPermitCount>();
        
        CreateMap<TransportPermit, TransportPermitDto>();
        CreateMap<TransportPermitCreateDto, TransportPermit>();
        
        CreateMap<TransitCountryCreateDto, TransitCountry>();
        CreateMap<TransitCountry, TransitCountryDto>();
        
        CreateMap<TransportCreateDto, Transport>();
        
        CreateMap<AdditionalPermissionCreateDto, AdditionalPermission>();
        CreateMap<AdditionalPermission, AdditionalPermissionDto>();
        
        CreateMap<Permit, PermitDto>();
        
        #endregion

        #region DriverCarrier

        CreateMap<DriverCarrier, DriverDto>()
            .ForMember(dest => dest.Contacts, src => src.MapFrom(v => v.User.Contacts))
            .ForMember(dest => dest.LastName, src => src.MapFrom(v => v.User.LastName))
            .ForMember(dest => dest.FirstName, src => src.MapFrom(v => v.User.FirstName))
            .ForMember(dest => dest.Username, src => src.MapFrom(v => v.User.Username))
            .ForMember(dest => dest.Email, src => src.MapFrom(v => v.User.Email))
            .ForMember(dest => dest.IsActive, src => src.MapFrom(v => v.User.IsActive))
            .ForMember(dest => dest.UserId, src => src.MapFrom(v => v.User.Id))
            ;

        #endregion
    }
}