﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.AdditionalPermission.Dto;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.TransitCountry.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.AdditionalPermission.Queries;

public class AdditionalPermissionListQuery: GetListQuery<AdditionalPermissionFilterDto, AdditionalPermissionDto>
{
    public class Handler: GetListQueryHandler<AdditionalPermissionListQuery, AdditionalPermissionFilterDto, 
        Entities.AdditionalPermission, AdditionalPermissionDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.AdditionalPermission> QueryData(AdditionalPermissionFilterDto filter, IQueryable<Entities.AdditionalPermission> query)
        {
            query = query.Where(x => filter.CountryCoupleYearPermitCountId == null
                                     || x.CountryCoupleYearPermitCountId == filter.CountryCoupleYearPermitCountId);
            return base.QueryData(filter, query);
        }
    }
}