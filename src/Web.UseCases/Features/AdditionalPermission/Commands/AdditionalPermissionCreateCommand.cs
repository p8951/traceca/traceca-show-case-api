﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.AdditionalPermission.Dto;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using Web.UseCases.Features.TransitCountry.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.AdditionalPermission.Commands;

public class AdditionalPermissionCreateCommand: CreateCommand<AdditionalPermissionCreateDto, int>
{
    public class Handler: CreateCommandHandler<AdditionalPermissionCreateCommand, Entities.AdditionalPermission, AdditionalPermissionCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<int> Handle(AdditionalPermissionCreateCommand request, CancellationToken cancellationToken)
        {
            var entity = await _dbContext.AdditionalPermissions
                .SingleOrDefaultAsync(
                    x => x.CountryCoupleYearPermitCountId == request.Dto.CountryCoupleYearPermitCountId,
                    cancellationToken);

            if (entity != null)
            {
                entity.Fields = request.Dto.Fields;
                _dbContext.AdditionalPermissions.Update(entity);
            }
            else
            {
                entity = new Entities.AdditionalPermission
                {
                    CountryCoupleYearPermitCountId = request.Dto.CountryCoupleYearPermitCountId,
                    Fields = request.Dto.Fields
                };
                await _dbContext.AdditionalPermissions.AddAsync(entity, cancellationToken);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
            return entity.Id;
        }
    }
}