﻿namespace Web.UseCases.Features.AdditionalPermission.Dto;

public class AdditionalPermissionFilterDto
{
    public int? CountryCoupleYearPermitCountId { get; set; }
}