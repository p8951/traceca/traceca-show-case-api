﻿using Utils.Json;

namespace Web.UseCases.Features.AdditionalPermission.Dto;

public class AdditionalPermissionCreateDto
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public List<AdditionalFieldJson> Fields { get; set; }
}