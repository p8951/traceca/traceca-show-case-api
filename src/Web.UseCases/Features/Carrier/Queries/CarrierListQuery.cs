﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Carrier.Dto;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Carrier.Queries;

public class CarrierListQuery: GetListQuery<CarrierFilterDto, CarrierDto>
{
    public class Handler: GetListQueryHandler<CarrierListQuery, CarrierFilterDto, Entities.Carrier, CarrierDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}