﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Carrier.Dto;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Carrier.Queries;

public class CarrierPagedListQuery: GetPagedListQuery<CarrierFilterDto, CarrierDto>
{
    public class Handler: GetPagedListQueryHandler<CarrierPagedListQuery, CarrierFilterDto, Entities.Carrier,
        CarrierDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.Carrier> QueryData(CarrierFilterDto filter, IQueryable<Entities.Carrier> query)
        {
            query = query
                .Include(c => c.Country)
                .Where(x => filter.CountryId == null || x.CountryId == filter.CountryId);
            return base.QueryData(filter, query);
        }
    }
}