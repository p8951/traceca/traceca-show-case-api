﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.Carrier.Dto;

public class CarrierFilterDto: BasePagedQuery
{
    public int? CountryId { get; set; }
}