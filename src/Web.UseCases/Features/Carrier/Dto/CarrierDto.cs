﻿using Utils;
using Utils.Json;
using Web.UseCases.Features.Dictionary.Country.Dto;

namespace Web.UseCases.Features.Carrier.Dto;

public class CarrierDto
{
    public int Id { get; set; }
    public TranslationNameJson Name { get; set; }
    public int CountryId { get; set; }
    public CountryDto Country { get; set; }
    public string Contacts { get; set; }
    public string Organization { get; set; }
    public string Email { get; set; }
    public string City { get; set; }
    public string Address { get; set; }
    public List<AdditionalFieldValueJson> AdditionalFieldValues { get; set; }
}