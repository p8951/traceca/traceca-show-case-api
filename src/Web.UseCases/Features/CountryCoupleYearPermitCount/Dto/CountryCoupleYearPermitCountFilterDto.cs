﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;

public class CountryCoupleYearPermitCountFilterDto: BasePagedQuery
{
    public short? Year { get; set; }
    public int? FirstCountryId { get; set; }
}