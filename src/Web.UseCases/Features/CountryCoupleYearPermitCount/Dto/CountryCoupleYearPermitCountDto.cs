﻿using Web.UseCases.Features.Dictionary.Country.Dto;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;

public class CountryCoupleYearPermitCountDto
{
    public int Id { get; set; }
    public int FirstCountryId { get; set; }
    public CountryDto FirstCountry { get; set; }
    public int SecondCountryId { get; set; }
    public CountryDto SecondCountry { get; set; }
    public short Year { get; set; }
    public int CountPermit { get; set; }
    public DateTime CreatedAt { get; set; }
}