﻿namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;

public class CountryCoupleYearPermitCountAddCountDto
{
    public int CountPermit { get; set; }
}