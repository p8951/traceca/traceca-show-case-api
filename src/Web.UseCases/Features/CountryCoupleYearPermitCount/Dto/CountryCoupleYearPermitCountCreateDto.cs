﻿using Utils.Json;
using Web.UseCases.Features.AdditionalPermission.Dto;
using Web.UseCases.Features.TransitCountry.Dto;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;

public class CountryCoupleYearPermitCountCreateDto
{
    public int FirstCountryId { get; set; }
    public int SecondCountryId { get; set; }
    public int CountPermit { get; set; }

    public List<int> TransitCountries { get; set; }

    public List<AdditionalFieldJson> AdditionalFields { get; set; }
}