﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Commands;

public class CountryCoupleYearPermitCountCreateCommand: CreateCommand<CountryCoupleYearPermitCountCreateDto, int>
{
    public class Handler: CreateCommandHandler<CountryCoupleYearPermitCountCreateCommand, Entities.CountryCoupleYearPermitCount, CountryCoupleYearPermitCountCreateDto, int>
    {
        private readonly ISender _sender;

        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService,
            ISender sender) : base(mapper, dbContext, httpContextService)
        {
            _sender = sender;
        }

        public override async Task<int> Handle(CountryCoupleYearPermitCountCreateCommand request, CancellationToken cancellationToken)
        {
            var currentYear = (short) DateTime.UtcNow.Year;
            var data = await _dbContext.CountryCoupleYearPermitCounts
                .Where(x => x.Year == currentYear
                            && x.FirstCountryId == request.Dto.FirstCountryId
                            && x.SecondCountryId == request.Dto.SecondCountryId)
                .SingleOrDefaultAsync(cancellationToken);
            if (data != null)
            {
                throw new AppException("Already exists");
            }

            var newCountryCoupleYear = new Entities.CountryCoupleYearPermitCount
            {
                Year = currentYear,
                CountPermit = request.Dto.CountPermit,
                FirstCountryId = request.Dto.FirstCountryId,
                SecondCountryId = request.Dto.SecondCountryId
            };
            await _dbContext.CountryCoupleYearPermitCounts.AddAsync(newCountryCoupleYear, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            if (request.Dto.TransitCountries != null && request.Dto.TransitCountries.Count > 0)
            {
                var oldTransits = await _dbContext.TransitCountries
                    .Where(x => x.CountryCoupleYearPermitCountId == newCountryCoupleYear.Id)
                    .ToListAsync(cancellationToken);
                if (oldTransits.Count > 0)
                {
                    _dbContext.TransitCountries.RemoveRange(oldTransits);
                    await _dbContext.SaveChangesAsync(cancellationToken);
                }
                var transitCountryList = new List<Entities.TransitCountry>();
                foreach (var countryId in request.Dto.TransitCountries)
                {
                    var newTransitCountry = new Entities.TransitCountry
                    {
                        CountryId = countryId,
                        CountryCoupleYearPermitCountId = newCountryCoupleYear.Id
                    };
                    transitCountryList.Add(newTransitCountry);
                }

                await _dbContext.TransitCountries.AddRangeAsync(transitCountryList, cancellationToken);

                var additionalPermission = await _dbContext.AdditionalPermissions.Where(x =>
                    x.CountryCoupleYearPermitCountId == newCountryCoupleYear.Id)
                    .SingleOrDefaultAsync(cancellationToken);
                if (additionalPermission == null)
                {
                    await _dbContext.AdditionalPermissions.AddAsync(new Entities.AdditionalPermission
                    {
                        CountryCoupleYearPermitCountId = newCountryCoupleYear.Id,
                        Fields = request.Dto.AdditionalFields
                    }, cancellationToken);
                }
                else
                {
                    additionalPermission.Fields = request.Dto.AdditionalFields;
                    _dbContext.AdditionalPermissions.Update(additionalPermission);
                }
                await _dbContext.SaveChangesAsync(cancellationToken);
            }
            
            // -----------
            var countryCouplePermitCounts = await _dbContext.CountryCouplePermitCounts
                .SingleOrDefaultAsync(
                    x => x.CountryCoupleYearPermitCountId == newCountryCoupleYear.Id,
                    cancellationToken);
            if (countryCouplePermitCounts != null)
            {
                throw new AppException("Country already exists");
            }

            var newCountry = new Entities.CountryCouplePermitCount
            {
                CountryCoupleYearPermitCountId = newCountryCoupleYear.Id,
                PermitCount = 0
            };

            await _dbContext.CountryCouplePermitCounts.AddAsync(newCountry, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return newCountryCoupleYear.Id;
        }

        protected override void InitializeNewEntity(Entities.CountryCoupleYearPermitCount entity)
        {
            entity.Year = (short) DateTime.UtcNow.Year;
        }
    }
}