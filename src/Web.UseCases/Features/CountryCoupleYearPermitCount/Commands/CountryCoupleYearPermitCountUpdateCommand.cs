﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Commands;

public class CountryCoupleYearPermitCountUpdateCommand: IRequest<Unit>
{
    public int Id { get; set; }
    public CountryCoupleYearPermitCountAddCountDto Dto { get; set; }
    public class Handler: BaseHandler<CountryCoupleYearPermitCountUpdateCommand, Unit>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<Unit> Handle(CountryCoupleYearPermitCountUpdateCommand request, CancellationToken cancellationToken)
        {
            var data = await _dbContext.CountryCoupleYearPermitCounts
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (data == null)
            {
                throw new KeyNotFoundException("Not found");
            }

            data.CountPermit += request.Dto.CountPermit;
            _dbContext.CountryCoupleYearPermitCounts.Update(data);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}