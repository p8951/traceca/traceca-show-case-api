﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Queries;

public class CountryCoupleYearPermitCountPagedListQuery: GetPagedListQuery<CountryCoupleYearPermitCountFilterDto, CountryCoupleYearPermitCountDto>
{
    public class Handler: GetPagedListQueryHandler<CountryCoupleYearPermitCountPagedListQuery, CountryCoupleYearPermitCountFilterDto, Entities.CountryCoupleYearPermitCount,
        CountryCoupleYearPermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.CountryCoupleYearPermitCount> QueryData(CountryCoupleYearPermitCountFilterDto filter, IQueryable<Entities.CountryCoupleYearPermitCount> query)
        {
            query = query
                .Include(c => c.FirstCountry)    
                .Include(c => c.SecondCountry)    
                .Where(x => filter.Year == null || x.Year == filter.Year)
                .Where(x => filter.FirstCountryId == null || x.FirstCountryId == filter.FirstCountryId);
            return base.QueryData(filter, query);
        }
    }
}