﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Queries.GetById;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCoupleYearPermitCount.Queries;

public class CountryCoupleYearPermitCountGetByIdQuery: GetByIdQuery<CountryCoupleYearPermitCountDto, int>
{
    public class Handler: GetByIdQueryHandler<CountryCoupleYearPermitCountGetByIdQuery, Entities.CountryCoupleYearPermitCount, CountryCoupleYearPermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.CountryCoupleYearPermitCount> QueryData(int id, IQueryable<Entities.CountryCoupleYearPermitCount> query)
        {
            query = query.Include(x => x.FirstCountry)
                .Include(x => x.SecondCountry);
            return base.QueryData(id, query);
        }
    }
}