﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.TransitCountry.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransitCountry.Commands;

public class TransitCountryCreateCommand: CreateCommand<TransitCountryCreateDto, int>
{
    public class Handler: CreateCommandHandler<TransitCountryCreateCommand, Entities.TransitCountry, TransitCountryCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}