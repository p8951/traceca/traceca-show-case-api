﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Commands.Delete;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransitCountry.Commands;

public class TransitCountryDeleteCommand : DeleteCommand<int>
{
    public class Handler : DeleteCommandHandler<TransitCountryDeleteCommand, Entities.TransitCountry, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}