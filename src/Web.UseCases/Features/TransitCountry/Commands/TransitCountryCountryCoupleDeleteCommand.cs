﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransitCountry.Commands;

public class TransitCountryCountryCoupleDeleteCommand: IRequest
{
    public int CountryCoupleYearPermitCountId { get; set; }
    
    public class Handler: BaseHandler<TransitCountryCountryCoupleDeleteCommand, Unit>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<Unit> Handle(TransitCountryCountryCoupleDeleteCommand request, CancellationToken cancellationToken)
        {
            var entities = await _dbContext.TransitCountries
                .Where(x => x.CountryCoupleYearPermitCountId == request.CountryCoupleYearPermitCountId)
                .ToListAsync(cancellationToken);
            _dbContext.TransitCountries.RemoveRange(entities);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}