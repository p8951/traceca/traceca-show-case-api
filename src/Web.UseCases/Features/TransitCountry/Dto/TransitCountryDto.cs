﻿using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using Web.UseCases.Features.Dictionary.Country.Dto;

namespace Web.UseCases.Features.TransitCountry.Dto;

public class TransitCountryDto
{
    public CountryCoupleYearPermitCountDto CountryCoupleYearPermitCount { get; set; }
    public CountryDto Country { get; set; }
}