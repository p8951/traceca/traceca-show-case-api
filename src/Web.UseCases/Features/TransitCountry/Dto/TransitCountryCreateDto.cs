﻿namespace Web.UseCases.Features.TransitCountry.Dto;

public class TransitCountryCreateDto
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public int CountryId { get; set; }
}