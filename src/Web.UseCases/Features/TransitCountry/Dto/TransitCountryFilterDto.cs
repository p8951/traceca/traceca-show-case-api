﻿namespace Web.UseCases.Features.TransitCountry.Dto;

public class TransitCountryFilterDto
{
    public int? CountryCoupleYearPermitCountId { get; set; }
}