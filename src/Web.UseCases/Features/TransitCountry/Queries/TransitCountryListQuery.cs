﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.TransitCountry.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransitCountry.Queries;

public class TransitCountryListQuery: GetListQuery<TransitCountryFilterDto, TransitCountryDto>
{
    public class Handler: GetListQueryHandler<TransitCountryListQuery, TransitCountryFilterDto, 
        Entities.TransitCountry, TransitCountryDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.TransitCountry> QueryData(TransitCountryFilterDto filter, IQueryable<Entities.TransitCountry> query)
        {
            query = query.Where(x => filter.CountryCoupleYearPermitCountId == null
                                     || x.CountryCoupleYearPermitCountId == filter.CountryCoupleYearPermitCountId);
            return base.QueryData(filter, query);
        }
    }
}