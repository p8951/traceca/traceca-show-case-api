﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CarrierPermitCount.Commands;

public class CarrierPermitCountCreateCommand: CreateCommand<CarrierPermitCountCreateDto, int>
{
    public class Handler: CreateCommandHandler<CarrierPermitCountCreateCommand, Entities.CarrierPermitCount, CarrierPermitCountCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<int> Handle(CarrierPermitCountCreateCommand request, CancellationToken cancellationToken)
        {
            var data = await _dbContext.CarrierPermitCounts
                .SingleOrDefaultAsync(
                    x => x.CarrierId == request.Dto.CarrierId  && x.CountryCouplePermitCountId == request.Dto.CountryCouplePermitCountId, 
                    cancellationToken);
            if (data != null)
            {
                throw new AppException("Already exists");
            }
            return await base.Handle(request, cancellationToken);
        }
    }
}