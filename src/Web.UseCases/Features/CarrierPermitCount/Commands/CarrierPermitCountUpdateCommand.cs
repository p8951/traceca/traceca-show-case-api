﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CarrierPermitCount.Commands;

public class CarrierPermitCountUpdateCommand: IRequest<Unit>
{
    public int Id { get; set; }
    public CarrierPermitCountAddCountDto Dto { get; set; }
    public class Handler: BaseHandler<CarrierPermitCountUpdateCommand, Unit>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<Unit> Handle(CarrierPermitCountUpdateCommand request, CancellationToken cancellationToken)
        {
            var data = await _dbContext.CarrierPermitCounts
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (data == null)
            {
                throw new KeyNotFoundException("Not found");
            }

            data.PermitCount += request.Dto.PermitCount;
            _dbContext.CarrierPermitCounts.Update(data);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}