﻿using Utils;

namespace Web.UseCases.Features.CarrierPermitCount.Dto;

public class CarrierPermitCountCreateDto
{
    public int CarrierId { get; set; }
    public int CountryCouplePermitCountId { get; set; }
    public int PermitCount { get; set; }
}