﻿namespace Web.UseCases.Features.CarrierPermitCount.Dto;

public class CarrierPermitCountAddCountDto
{
    public int PermitCount { get; set; }
}