﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.CarrierPermitCount.Dto;

public class CarrierPermitCountFilterDto: BasePagedQuery
{
    public int? CountryCouplePermitCountId { get; set; }
    public int? FirstCountryId { get; set; }
    public int? SecondCountryId { get; set; }
    public int? CarrierId { get; set; }
}