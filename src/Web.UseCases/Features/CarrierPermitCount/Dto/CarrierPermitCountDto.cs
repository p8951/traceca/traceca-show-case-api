﻿using Web.UseCases.Features.Carrier.Dto;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;

namespace Web.UseCases.Features.CarrierPermitCount.Dto;

public class CarrierPermitCountDto
{
    public int Id { get; set; }
    public int CarrierId { get; set; }
    public CarrierDto Carrier { get; set; }
    public int CountryCouplePermitCountId { get; set; }
    public CountryCouplePermitCountDto CountryCouplePermitCount { get; set; }
    public int PermitCount { get; set; }
}