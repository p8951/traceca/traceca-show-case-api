﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Dictionary;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CarrierPermitCount.Queries;

public class CarrierPermitCountPagedListQuery: GetPagedListQuery<CarrierPermitCountFilterDto, CarrierPermitCountDto>
{
    public class Handler: GetPagedListQueryHandler<CarrierPermitCountPagedListQuery, CarrierPermitCountFilterDto, Entities.CarrierPermitCount,
        CarrierPermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.CarrierPermitCount> QueryData(CarrierPermitCountFilterDto filter, IQueryable<Entities.CarrierPermitCount> query)
        {
            query = query
                .Include(c => c.CountryCouplePermitCount).ThenInclude(v => v.CountryCoupleYearPermitCount)
                .Where(x => (filter.CountryCouplePermitCountId == null
                             || x.CountryCouplePermitCountId == filter.CountryCouplePermitCountId)
                            && (filter.CarrierId == null
                                || x.CarrierId == filter.CarrierId)
                            && (filter.FirstCountryId == null
                                || x.CountryCouplePermitCount.CountryCoupleYearPermitCount.FirstCountryId == filter.FirstCountryId)
                            && (filter.SecondCountryId == null
                                || x.CountryCouplePermitCount.CountryCoupleYearPermitCount.SecondCountryId == filter.SecondCountryId));
            return base.QueryData(filter, query);
        }
    }
}