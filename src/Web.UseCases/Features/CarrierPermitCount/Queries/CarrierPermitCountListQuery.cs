﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetList;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CarrierPermitCount.Queries;

public class CarrierPermitCountListQuery: GetListQuery<CarrierPermitCountFilterDto, CarrierPermitCountDto>
{
    public class Handler: GetListQueryHandler<CarrierPermitCountListQuery, CarrierPermitCountFilterDto, Entities.CarrierPermitCount, CarrierPermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}