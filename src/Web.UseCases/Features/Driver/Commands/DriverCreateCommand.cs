﻿using Auth.Interfaces;
using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Utils.Enums;
using Utils.Exceptions;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.Driver.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Driver.Commands;

public class DriverCreateCommand: CreateCommand<DriverCreateDto, int>
{
    public class Handler: BaseHandler<DriverCreateCommand, int>
    {
        private readonly JwtAppSettings _jwtAppSettings;

        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService,
            IOptions<JwtAppSettings> jwtAppSettings) : base(mapper, dbContext, httpContextService)
        {
            _jwtAppSettings = jwtAppSettings.Value;
        }

        public override async  Task<int> Handle(DriverCreateCommand request, CancellationToken cancellationToken)
        {
            var haveUser = await _dbContext.Users.Where(x => x.Username == request.Dto.UserName)
                .FirstOrDefaultAsync(cancellationToken);
            
            var currentUser = await _dbContext.Users.Where(x => x.Id == _httpContextService.UserId)
                .SingleAsync(cancellationToken);

            if (haveUser != null && haveUser.IsActive == true)
            {
                throw new AppException("Username already exists");
            }
            if (haveUser != null && haveUser.IsActive == false)
            {
                throw new AppException("Username already exists, but not activate. Please activate this driver");
            }
            
            var role = await _dbContext.Roles.SingleAsync(x => x.Id == RoleEnum.Transport, cancellationToken);
            var newUserRoles = new List<Role>();
            newUserRoles.Add(role);
            var newUser = new User
            {
                Username = request.Dto.UserName,
                Email = request.Dto.Email,
                LastName = request.Dto.LastName,
                FirstName = request.Dto.FirstName,
                Patronymic = request.Dto.Patronymic ?? "",
                Contacts = request.Dto.Contacts,
                PasswordHash = BCrypt.Net.BCrypt.HashPassword(request.Dto.Password, _jwtAppSettings.Salt),
                Roles = newUserRoles,
                IsActive = true,
                CountryId = currentUser.CountryId,
                Organization = ""
            };
            await _dbContext.Users.AddAsync(newUser, cancellationToken);

            await _dbContext.SaveChangesAsync(cancellationToken);

            var driverCarrier = new DriverCarrier
            {
                CarrierId = currentUser.CarrierId.Value,
                UserId = newUser.Id
            };
            await _dbContext.DriverCarriers.AddAsync(driverCarrier, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return newUser.Id;
        }
    }
}