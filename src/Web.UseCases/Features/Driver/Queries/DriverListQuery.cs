﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.Driver.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Driver.Queries;

public class DriverListQuery: GetListQuery<DriverFilterDto, DriverDto>
{
    public class Handler: GetListQueryHandler<DriverListQuery, DriverFilterDto, Entities.DriverCarrier, DriverDto, long>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<DriverCarrier> QueryData(DriverFilterDto filter, IQueryable<DriverCarrier> query)
        {
            query = query
                .Include(x => x.User)
                .Where(x => filter.CarrierId == null || x.CarrierId == filter.CarrierId);
            return base.QueryData(filter, query);
        }
    }
}