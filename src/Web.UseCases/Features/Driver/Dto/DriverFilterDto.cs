﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.Driver.Dto;

public class DriverFilterDto: BasePagedQuery
{
    public int? CarrierId { get; set; }
}