﻿namespace Web.UseCases.Features.Driver.Dto;

public class DriverCreateDto
{
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string? Patronymic { get; set; }

    public string UserName { get; set; }
    public string Email { get; set; }
    public string Contacts { get; set; }
    public string Password { get; set; }
}