﻿namespace Web.UseCases.Features.Driver.Dto;

public class DriverDto
{
    public int UserId { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string? Patronymic { get; set; }

    public string Username { get; set; }
    public string Email { get; set; }
    public string Contacts { get; set; }
    public bool IsActive { get; set; }
}