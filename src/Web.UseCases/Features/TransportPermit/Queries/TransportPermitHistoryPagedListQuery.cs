﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransportPermit.Queries;

public class TransportPermitHistoryPagedListQuery: GetPagedListQuery<TransportPermitHistoryFilterDto, TransportPermitDto>
{
    public class Handler: GetPagedListQueryHandler<TransportPermitHistoryPagedListQuery, TransportPermitHistoryFilterDto, Entities.TransportPermit,
        TransportPermitDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
        
        protected override IQueryable<Entities.TransportPermit> QueryData(TransportPermitHistoryFilterDto filter, IQueryable<Entities.TransportPermit> query)
        {
            query = query
                .Include(c => c.Permit).ThenInclude(c => c.CountryCoupleYearPermitCount)
                    .ThenInclude(b =>b.FirstCountry)
                .Include(c => c.Permit).ThenInclude(b => b.CountryCoupleYearPermitCount)
                    .ThenInclude(v => v.SecondCountry)
                .Include(c => c.Transport).ThenInclude(v => v.Carrier)
                .Where(x => (filter.PermitCode == null || x.Permit.PermitCode.ToLower().Contains(filter.PermitCode.ToLower()))
                            && (filter.FirstCountryId == null || x.Permit.CountryCoupleYearPermitCount.FirstCountryId == filter.FirstCountryId)
                            && (filter.SecondCountryId == null || x.Permit.CountryCoupleYearPermitCount.SecondCountryId == filter.SecondCountryId)
                            );
                
            return base.QueryData(filter, query);
        }
    }
}