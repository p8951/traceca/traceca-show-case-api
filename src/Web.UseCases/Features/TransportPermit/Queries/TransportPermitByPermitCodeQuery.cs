﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransportPermit.Queries;

public class TransportPermitByPermitCodeQuery : IRequest<TransportPermitDto>
{
    public string PermitCode { get; set; }

    public class Handler : BaseHandler<TransportPermitByPermitCodeQuery, TransportPermitDto>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper,
            dbContext, httpContextService)
        {
        }

        public override async Task<TransportPermitDto> Handle(TransportPermitByPermitCodeQuery request,
            CancellationToken cancellationToken)
        {
            var transportPermit = await _dbContext.TransportPermits
                .Include(c => c.Permit)
                .ThenInclude(c => c.CountryCoupleYearPermitCount)
                .ThenInclude(c => c.FirstCountry)
                .Include(c => c.Permit)
                .ThenInclude(c => c.CountryCoupleYearPermitCount)
                .ThenInclude(c => c.SecondCountry)
                .Include(c => c.Transport)
                .ThenInclude(c => c.Carrier)
                .Include(c => c.Transport)
                .ThenInclude(c => c.User)
                .Where(x => x.Permit.PermitCode.ToLower() == request.PermitCode.ToLower())
                .AsNoTracking()
                .ProjectTo<TransportPermitDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);
            if (transportPermit == null)
            {
                throw new KeyNotFoundException("PermitCode not found");
            }
            return transportPermit;
        }
    }
}