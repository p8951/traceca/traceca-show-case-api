﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransportPermit.Queries;

public class TransportPermitListQuery: GetListQuery<TransportPermitFilterDto, TransportPermitDto>
{
    public class Handler: GetListQueryHandler<TransportPermitListQuery, TransportPermitFilterDto, Entities.TransportPermit, TransportPermitDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.TransportPermit> QueryData(TransportPermitFilterDto filter, IQueryable<Entities.TransportPermit> query)
        {
            query = query
                .Include(c => c.Permit).ThenInclude(c => c.CountryCoupleYearPermitCount)
                .ThenInclude(b =>b.FirstCountry)
                .Include(c => c.Transport)
                .Where(x => (filter.CarrierId == null || x.Transport.CarrierId == filter.CarrierId)
                            && (filter.SecondCountryId == null || x.Permit.CountryCoupleYearPermitCount.SecondCountryId == filter.SecondCountryId)
                            && (filter.UserId == null || x.Transport.UserId == filter.UserId)
                            );
                
            return base.QueryData(filter, query);
        }
    }
}