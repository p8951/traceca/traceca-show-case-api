﻿using Web.UseCases.Features.Permit.Dto;
using Web.UseCases.Features.Transport.Dto;

namespace Web.UseCases.Features.TransportPermit.Dto;

public class TransportPermitDto
{
    public int Id { get; set; }
    public int TransportId { get; set; }
    public TransportDto Transport { get; set; }
    public long PermitId { get; set; }
    public PermitDto Permit { get; set; }
    public bool Used { get; set; }
    public DateTime CreatedAt { get; set; }
}