﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.TransportPermit.Dto;

public class TransportPermitFilterDto: BasePagedQuery
{
    public int? CarrierId { get; set; }
    public int? SecondCountryId { get; set; }
    public int? UserId { get; set; }
}