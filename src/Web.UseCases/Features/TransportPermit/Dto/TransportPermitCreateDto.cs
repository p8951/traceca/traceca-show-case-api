﻿namespace Web.UseCases.Features.TransportPermit.Dto;

public class TransportPermitCreateDto
{
    public int TransportId { get; set; }
    public int CountryCoupleYearPermitCountId { get; set; }
}