﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.TransportPermit.Dto;

public class TransportPermitHistoryFilterDto: BasePagedQuery
{
    public string? PermitCode { get; set; }
    public int? FirstCountryId { get; set; }
    public int? SecondCountryId { get; set; }
}