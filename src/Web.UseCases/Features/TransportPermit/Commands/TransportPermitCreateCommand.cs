﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.TransportPermit.Commands;

public class TransportPermitCreateCommand : CreateCommand<TransportPermitCreateDto, int>
{
    public class Handler : CreateCommandHandler<TransportPermitCreateCommand, Entities.TransportPermit,
        TransportPermitCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper,
            dbContext, httpContextService)
        {
        }

        public override async Task<int> Handle(TransportPermitCreateCommand request,
            CancellationToken cancellationToken)
        {
            var permit = await _dbContext.Permits
                .Where(x => x.Used == false &&
                            x.CountryCoupleYearPermitCountId == request.Dto.CountryCoupleYearPermitCountId)
                .OrderBy(x => x.Id)
                .FirstOrDefaultAsync(cancellationToken);
            if (permit == null)
            {
                throw new AppException("Active permit not found");
            }

            var transportPermit = new Entities.TransportPermit
            {
                TransportId = request.Dto.TransportId,
                PermitId = permit.Id
            };
            await _dbContext.TransportPermits.AddAsync(transportPermit, cancellationToken);
            permit.Used = true;
            _dbContext.Permits.Update(permit);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return transportPermit.Id;
        }
    }
}