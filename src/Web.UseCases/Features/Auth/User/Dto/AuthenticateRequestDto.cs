﻿using System.ComponentModel.DataAnnotations;

namespace Web.UseCases.Features.Auth.User.Dto;

public class AuthenticateRequestDto
{
    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }
}