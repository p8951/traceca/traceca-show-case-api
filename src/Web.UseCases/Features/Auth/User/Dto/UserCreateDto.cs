﻿using Utils.Enums;

namespace Web.UseCases.Features.Auth.User.Dto;

public class UserCreateDto
{
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string? Patronymic { get; set; }

    public string Iin { get; set; }
    public string UserName { get; set; }
    public string Email { get; set; }
    public List<RoleEnum> Roles { get; set; }
    public string Contacts { get; set; }
}