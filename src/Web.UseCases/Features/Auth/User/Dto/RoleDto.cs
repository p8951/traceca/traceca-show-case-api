﻿using Utils.Enums;

namespace Web.UseCases.Features.Auth.User.Dto;

public class RoleDto
{
    public RoleEnum Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
}