﻿using Web.UseCases.Features.Dictionary.Country.Dto;

namespace Web.UseCases.Features.Auth.User.Dto;

public class UserDto
{
    public int Id { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Patronymic { get; set; }
    public virtual string FullName => LastName + ' ' + FirstName + ' ' + Patronymic;

    public string Iin { get; set; }
    public string UserName { get; set; }
    public string Email { get; set; }
    public bool IsActive { get; set; } = true;
    public List<RoleDto> Roles { get; set; }
    public CountryDto Country { get; set; }
    public int CountryId { get; set; }
    public int? CarrierId { get; set; }
}