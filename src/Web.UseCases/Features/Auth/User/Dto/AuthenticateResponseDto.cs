﻿using System.Text.Json.Serialization;
using Auth.Interfaces.Dto;

namespace Web.UseCases.Features.Auth.User.Dto;

public class AuthenticateResponseDto
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Username { get; set; }
    public string JwtToken { get; set; }
    public int CountryId { get; set; }
    public List<AuthRoleDto> Roles { get; set; }

    [JsonIgnore] // refresh token is returned in http only cookie
    public string RefreshToken { get; set; }

}