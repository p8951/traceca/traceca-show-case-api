﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Common.Commands.Create;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Auth.User.Commands;

public class UserCreateCommand: CreateCommand<UserCreateDto, int>
{
    public class Handler: CreateCommandHandler<UserCreateCommand, Entities.Auth.User, UserCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}