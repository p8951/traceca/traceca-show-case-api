﻿using Auth.Interfaces;
using Auth.Interfaces.Dto;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Auth;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Utils.Exceptions;
using Web.UseCases.Features.Auth.User.Dto;

namespace Web.UseCases.Features.Auth.User.Commands;

public class UserAuthenticateCommand: IRequest<AuthenticateResponseDto>
{
    public AuthenticateRequestDto Dto { get; set; }
    public string IpAddress { get; set; }
    
    public class Handler: IRequestHandler<UserAuthenticateCommand, AuthenticateResponseDto>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IJwtUtils _jwtUtils;
        private readonly ISender _sender;

        public Handler(IDbContext dbContext, IMapper mapper, IJwtUtils jwtUtils, 
            IOptions<JwtAppSettings> jwtAppSettings, ISender sender)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _jwtUtils = jwtUtils;
            _sender = sender;
        }
        public async Task<AuthenticateResponseDto> Handle(UserAuthenticateCommand request, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users
                .Include(x => x.Roles)
                .SingleOrDefaultAsync(x => x.Username == request.Dto.Username, cancellationToken);
            if (user == null || !BCrypt.Net.BCrypt.Verify(request.Dto.Password, user.PasswordHash))
                throw new AppException("Username or password is incorrect");
            var userDto = _mapper.Map<AuthUserDto>(user);
            var jwtToken = _jwtUtils.GenerateJwtToken(userDto);
            var refreshToken = _jwtUtils.GenerateRefreshToken(request.IpAddress);
            var refreshTokenEn = _mapper.Map<RefreshToken>(refreshToken) ;
            user.RefreshTokens.Add(refreshTokenEn);
            await _sender.Send(new RemoveOldRefreshTokenCommand {User = user}, cancellationToken);
            _dbContext.Users.Update(user);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return new AuthenticateResponseDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.Username,
                JwtToken = jwtToken,
                RefreshToken = refreshToken.Token,
                Roles = userDto.Roles,
                CountryId = user.CountryId
            };
        }
        
    }
}