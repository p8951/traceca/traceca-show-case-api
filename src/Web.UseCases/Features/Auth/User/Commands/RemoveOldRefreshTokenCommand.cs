﻿using Auth.Interfaces;
using MediatR;
using Microsoft.Extensions.Options;

namespace Web.UseCases.Features.Auth.User.Commands;

public class RemoveOldRefreshTokenCommand: IRequest<Unit>
{
    public Entities.Auth.User User { get; set; }
    
    public class Handler: IRequestHandler<RemoveOldRefreshTokenCommand, Unit>
    {
        private readonly JwtAppSettings _jwtAppSettings;

        public Handler(IOptions<JwtAppSettings> jwtAppSettings)
        {
            _jwtAppSettings = jwtAppSettings.Value;
        }
        public async Task<Unit> Handle(RemoveOldRefreshTokenCommand request, CancellationToken cancellationToken)
        {
            request.User.RefreshTokens.RemoveAll(x => 
                !x.IsActive && 
                x.CreatedAt.AddDays(_jwtAppSettings.RefreshTokenTTL) <= DateTime.UtcNow);
            return Unit.Value;
        }
    }
}