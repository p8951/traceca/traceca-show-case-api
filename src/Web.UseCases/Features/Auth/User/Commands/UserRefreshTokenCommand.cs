﻿using Auth.Interfaces;
using Auth.Interfaces.Dto;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Auth;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Utils.Exceptions;
using Web.UseCases.Features.Auth.User.Dto;

namespace Web.UseCases.Features.Auth.User.Commands;

public class UserRefreshTokenCommand: IRequest<AuthenticateResponseDto>
{
    public string? RefreshToken { get; set; }
    public string IpAddress { get; set; }
    
    public class Handler: IRequestHandler<UserRefreshTokenCommand, AuthenticateResponseDto>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IJwtUtils _jwtUtils;
        private readonly IOptions<JwtAppSettings> _jwtAppSettings;
        private readonly ISender _sender;

        public Handler(IDbContext dbContext, IMapper mapper, IJwtUtils jwtUtils, 
            IOptions<JwtAppSettings> jwtAppSettings, ISender sender)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _jwtUtils = jwtUtils;
            _jwtAppSettings = jwtAppSettings;
            _sender = sender;
        }
        public async Task<AuthenticateResponseDto> Handle(UserRefreshTokenCommand request, CancellationToken cancellationToken)
        {
            var user = await GetUserByRefreshToken(request.RefreshToken);
            var refreshToken = user.RefreshTokens.Single(x => x.Token == request.RefreshToken);

            if (refreshToken.IsRevoked)
            {
                // revoke all descendant tokens in case this token has been compromised
                RevokeDescendantRefreshTokens(refreshToken, user, request.IpAddress, $"Attempted reuse of revoked ancestor token: {request.RefreshToken}");
                _dbContext.Users.Update(user);
                await _dbContext.SaveChangesAsync(cancellationToken);
            }

            if (!refreshToken.IsActive)
                throw new AppException("Invalid token");

            // replace old refresh token with a new one (rotate token)
            var newRefreshToken = RotateRefreshToken(refreshToken, request.IpAddress);
            user.RefreshTokens.Add(newRefreshToken);

            // remove old refresh tokens from user
            await _sender.Send(new RemoveOldRefreshTokenCommand { User = user}, cancellationToken);

            // save changes to db
            _dbContext.Users.Update(user);
            await _dbContext.SaveChangesAsync(cancellationToken);

            // generate new jwt
            var userDto = _mapper.Map<AuthUserDto>(user);
            var jwtToken = _jwtUtils.GenerateJwtToken(userDto);

            return new AuthenticateResponseDto
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.Username,
                JwtToken = jwtToken,
                RefreshToken = refreshToken.Token
            };
        }
        
        private async Task<Entities.Auth.User>  GetUserByRefreshToken(string? token)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.RefreshTokens.Any(t => t.Token == token));

            if (user == null)
                throw new AppException("Invalid token");

            return user;
        }
        private void RevokeDescendantRefreshTokens(RefreshToken refreshToken, Entities.Auth.User user, string ipAddress, string reason)
        {
            // recursively traverse the refresh token chain and ensure all descendants are revoked
            if(!string.IsNullOrEmpty(refreshToken.ReplacedByToken))
            {
                var childToken = user.RefreshTokens.SingleOrDefault(x => x.Token == refreshToken.ReplacedByToken);
                if (childToken.IsActive)
                    revokeRefreshToken(childToken, ipAddress, reason);
                else
                    RevokeDescendantRefreshTokens(childToken, user, ipAddress, reason);
            }
        }

        private void revokeRefreshToken(RefreshToken token, string ipAddress, string reason = null, string replacedByToken = null)
        {
            token.Revoked = DateTime.UtcNow;
            token.RevokedByIp = ipAddress;
            token.ReasonRevoked = reason;
            token.ReplacedByToken = replacedByToken;
        }
        private RefreshToken RotateRefreshToken(RefreshToken refreshToken, string ipAddress)
        {
            var newRefreshTokenDto = _jwtUtils.GenerateRefreshToken(ipAddress);
            var newRefreshToken = _mapper.Map<RefreshToken>(newRefreshTokenDto);
            revokeRefreshToken(refreshToken, ipAddress, "Replaced by new token", newRefreshToken.Token);
            return newRefreshToken;
        }
    }
}