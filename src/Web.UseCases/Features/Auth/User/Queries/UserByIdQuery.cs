﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Common.Queries.GetById;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Auth.User.Queries;

public class UserByIdQuery: GetByIdQuery<UserDto, int>
{
    public class Handler: GetByIdQueryHandler<UserByIdQuery, Entities.Auth.User, UserDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.Auth.User> QueryData(int id, IQueryable<Entities.Auth.User> query)
        {
            query = query.Include(x => x.Roles)
                .Include(c => c.Country)
                .Where(x => x.IsActive == true);
            return base.QueryData(id, query);
        }
    }
}