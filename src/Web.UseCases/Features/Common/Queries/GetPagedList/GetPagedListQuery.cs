﻿using DataAccess.Interfaces;
using MediatR;
using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.Common.Queries.GetPagedList;

public abstract class GetPagedListQuery<TRequest, TResponse>: IRequest<PagedList<TResponse>>
{
    public TRequest Filter { get; set; }
}