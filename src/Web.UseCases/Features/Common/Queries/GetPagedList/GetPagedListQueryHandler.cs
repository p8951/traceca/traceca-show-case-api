﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Paged;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Queries.GetPagedList;

public abstract class
    GetPagedListQueryHandler<TCommand, TRequest, TEntity, TResponse, TIdType> : IRequestHandler<TCommand, PagedList<TResponse>>
    where TEntity : BaseEntity<TIdType>
    where TCommand : GetPagedListQuery<TRequest, TResponse>
    where TRequest : BasePagedQuery
{
    private readonly IMapper _mapper;
    private readonly IDbContext _dbContext;
    private readonly IHttpContextService _httpContextService;

    protected GetPagedListQueryHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
    {
        _mapper = mapper;
        _dbContext = dbContext;
        _httpContextService = httpContextService;
    }

    public async Task<PagedList<TResponse>> Handle(TCommand request, CancellationToken cancellationToken)
    {
        var query = _dbContext.Set<TEntity>().AsQueryable();
        query = QueryData(request.Filter, query);
        return await query
            .AsNoTracking()
            .ProjectTo<TResponse>(_mapper.ConfigurationProvider)
            .ToPagedListAsync(request.Filter.PageNumber, request.Filter.PageSize);
    }

    protected virtual IQueryable<TEntity> QueryData(TRequest filter, IQueryable<TEntity> query)
    {
        return query;
    }
}