﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Queries.GetList
{
    public abstract class GetListQueryHandler<TCommand, TRequest, TEntity, TResponse, TIdType>: IRequestHandler<TCommand, List<TResponse>>
        where TEntity : BaseEntity<TIdType>
        where TCommand : GetListQuery<TRequest, TResponse>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected GetListQueryHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }
        public virtual async Task<List<TResponse>> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var query = _dbContext.Set<TEntity>().AsQueryable();
            query = QueryData(request.Filter, query);
            return await query
                .AsNoTracking()
                .ProjectTo<TResponse>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
        
        protected virtual IQueryable<TEntity> QueryData(TRequest filter, IQueryable<TEntity> query)
        {
            return query;
        }
    }
}