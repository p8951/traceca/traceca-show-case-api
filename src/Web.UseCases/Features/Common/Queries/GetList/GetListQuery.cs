﻿using System.Collections.Generic;
using MediatR;

namespace Web.UseCases.Features.Common.Queries.GetList
{
    public abstract class GetListQuery<TRequest, TResponse>: IRequest<List<TResponse>>
    {
        public TRequest Filter { get; set; }
    }
}