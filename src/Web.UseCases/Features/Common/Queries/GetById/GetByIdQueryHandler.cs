﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using Microsoft.EntityFrameworkCore;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Queries.GetById
{
    public abstract class GetByIdQueryHandler<TRequest, TEntity, TDto, TIdType>: IRequestHandler<TRequest, TDto>
        where TEntity : BaseEntity<TIdType>
        where TRequest : GetByIdQuery<TDto, TIdType>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected GetByIdQueryHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }
        public async Task<TDto> Handle(TRequest request, CancellationToken cancellationToken)
        {
            var query = _dbContext.Set<TEntity>().AsQueryable();
            query = QueryData(request.Id, query);
            var data = await query
                .Where(x => x.Id != null && x.Id.Equals(request.Id))
                .ProjectTo<TDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(cancellationToken);
            if (data == null)
            {
                throw new KeyNotFoundException("Key not found");
            }
            return data;
        }
        
        protected virtual IQueryable<TEntity> QueryData(TIdType id, IQueryable<TEntity> query)
        {
            return query;
        }
    }
}