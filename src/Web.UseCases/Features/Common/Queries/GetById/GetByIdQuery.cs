﻿using System;
using MediatR;

namespace Web.UseCases.Features.Common.Queries.GetById
{
    public abstract class GetByIdQuery<TResponse, TIdType>: IRequest<TResponse>
    { 
        public TIdType Id { get; set; }
    }
}