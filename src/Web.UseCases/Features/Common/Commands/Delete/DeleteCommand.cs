﻿using System;
using MediatR;

namespace Web.UseCases.Features.Common.Commands.Delete
{
    public class DeleteCommand<TIdType>: IRequest
    {
        public TIdType Id { get; set; }
    }
}