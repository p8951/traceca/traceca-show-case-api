﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Commands.Delete
{
    public abstract class DeleteCommandHandler<TCommand, TEntity, TIdType>: IRequestHandler<TCommand>
        where TCommand : DeleteCommand<TIdType>
        where TEntity : BaseEntity<TIdType>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected DeleteCommandHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }
        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var entity = await GetTrackedEntityAsync(request.Id);
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
        
        protected virtual async Task<TEntity> GetTrackedEntityAsync(TIdType id)
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);
            if (entity == null)
            {
                throw new KeyNotFoundException("Key not found");
            }
            return entity;
        }
    }
}