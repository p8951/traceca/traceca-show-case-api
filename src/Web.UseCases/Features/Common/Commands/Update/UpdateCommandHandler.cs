﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Commands.Update
{
    public abstract class UpdateCommandHandler<TCommand, TEntity, TDto, TIdType> : IRequestHandler<TCommand>
        where TEntity : BaseEntity<TIdType>
        where TCommand : UpdateCommand<TDto, TIdType>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected UpdateCommandHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }
        
        public virtual async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var entity = await GetTrackedEntityAsync(request.Id);
            _mapper.Map(request.Dto, entity);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }

        protected virtual async Task<TEntity> GetTrackedEntityAsync(TIdType id)
        {
            var entity = await _dbContext.Set<TEntity>().FindAsync(id);
            if (entity == null)
            {
                throw new KeyNotFoundException("Key not found");
            }
            return entity;
        }
    }
}