﻿using System;
using MediatR;

namespace Web.UseCases.Features.Common.Commands.Update
{
    public abstract class UpdateCommand<TDto, TIdType>: IRequest
    {
        public TIdType Id { get; set; }
        public TDto Dto { get; set; }
    }
}