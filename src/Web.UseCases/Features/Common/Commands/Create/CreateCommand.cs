﻿using System;
using MediatR;

namespace Web.UseCases.Features.Common.Commands.Create
{
    public abstract class CreateCommand<TRequest, TIdType>: IRequest<TIdType>
    {
        public TRequest Dto { get; set; }
    }
}