﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities.Common;
using MediatR;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common.Commands.Create;

public abstract class CreateCommandHandler<TCommand, TEntity, TRequestDto, TIdType> : IRequestHandler<TCommand, TIdType>
    where TCommand : CreateCommand<TRequestDto, TIdType>
    where TEntity : BaseEntity<TIdType>
{
    protected readonly IMapper _mapper;
    protected readonly IDbContext _dbContext;
    protected readonly IHttpContextService _httpContextService;

    protected CreateCommandHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
    {
        _mapper = mapper;
        _dbContext = dbContext;
        _httpContextService = httpContextService;
    }
        
    public virtual async Task<TIdType> Handle(TCommand request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<TEntity>(request.Dto);
        InitializeNewEntity(entity);
        await _dbContext.Set<TEntity>().AddAsync(entity, cancellationToken);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return entity.Id;
    }
        
    protected virtual void InitializeNewEntity(TEntity entity)
    {
    }
}
