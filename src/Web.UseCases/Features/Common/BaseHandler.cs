﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Common
{
    public abstract class BaseHandler<TCommand, TResponse> : IRequestHandler<TCommand, TResponse>
        where TCommand : IRequest<TResponse>
    {
        protected readonly IMapper _mapper;
        protected readonly IDbContext _dbContext;
        protected readonly IHttpContextService _httpContextService;

        protected BaseHandler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextService = httpContextService;
        }

        public abstract Task<TResponse> Handle(TCommand request, CancellationToken cancellationToken);

    }
}