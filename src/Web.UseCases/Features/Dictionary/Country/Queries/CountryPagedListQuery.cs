﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.Dictionary.Country.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Dictionary.Country.Queries;

public class CountryPagedListQuery: GetPagedListQuery<CountryFilterDto, CountryDto>
{
    public class Handler: GetPagedListQueryHandler<CountryPagedListQuery, CountryFilterDto, Entities.Dictionary.Country, CountryDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}