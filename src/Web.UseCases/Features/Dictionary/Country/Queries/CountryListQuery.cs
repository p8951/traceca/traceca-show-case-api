﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.Dictionary.Country.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Dictionary.Country.Queries;

public class CountryListQuery: GetListQuery<CountryFilterDto, CountryDto>
{
    public class Handler: GetListQueryHandler<CountryListQuery, CountryFilterDto, Entities.Dictionary.Country, CountryDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}