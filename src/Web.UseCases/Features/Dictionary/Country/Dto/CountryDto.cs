﻿using Utils;

namespace Web.UseCases.Features.Dictionary.Country.Dto;

public class CountryDto
{
    public int Id { get; set; }
    public TranslationNameJson Name { get; set; }
    public DateTime CreatedAt { get; set; }
    public string Alpha2 { get; set; }
    public string Alpha3 { get; set; }
    public short Iso { get; set; }    
    /// <summary>
    /// Дата последнего изменения сущности
    /// </summary>
    public DateTime UpdatedAt { get; set; }
}