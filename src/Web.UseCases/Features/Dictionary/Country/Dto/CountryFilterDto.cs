﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.Dictionary.Country.Dto;

public class CountryFilterDto: BasePagedQuery
{
    public string? Name { get; set; }
}