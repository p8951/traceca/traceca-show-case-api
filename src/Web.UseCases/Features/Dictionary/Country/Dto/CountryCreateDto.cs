﻿using Utils;

namespace Web.UseCases.Features.Dictionary.Country.Dto;

public class CountryCreateDto
{
    public TranslationNameJson Name { get; set; }
}