﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.Dictionary.Country.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Dictionary.Country.Commands;

public class CountryCreateCommand: CreateCommand<CountryCreateDto, int>
{
    public class Handler: CreateCommandHandler<CountryCreateCommand, Entities.Dictionary.Country, CountryCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}