﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Commands.Update;
using Web.UseCases.Features.Dictionary.Country.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Dictionary.Country.Commands;

public class CountryUpdateCommand: UpdateCommand<CountryDto, int>
{
    public class Handler: UpdateCommandHandler<CountryUpdateCommand, Entities.Dictionary.Country, CountryDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}