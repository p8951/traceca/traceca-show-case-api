﻿using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;

namespace Web.UseCases.Features.Permit.Dto;

public class PermitDto
{
    public long Id { get; set; }
    public int CountryCoupleYearPermitCountId { get; set; }
    public CountryCoupleYearPermitCountDto CountryCoupleYearPermitCount { get; set; }
    public string PermitCode { get; set; }
    public bool Used { get; set; } = false;
}