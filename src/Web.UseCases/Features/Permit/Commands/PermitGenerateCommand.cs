﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Permit.Commands;

public class PermitGenerateCommand: IRequest<Unit>
{
    public CountryCoupleYearPermitCountDto CountryCoupleYearPermitCount { get; set; }
    public int startAt { get; set; }
    
    public class Handler: BaseHandler<PermitGenerateCommand, Unit>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<Unit> Handle(PermitGenerateCommand request, CancellationToken cancellationToken)
        {
            var permitList = new List<Entities.Permit>();
            for (int i = request.startAt; i < request.CountryCoupleYearPermitCount.CountPermit + 1; i++)
            {
                var permitCode = request.CountryCoupleYearPermitCount.FirstCountry.Alpha2
                                 + "-" + request.CountryCoupleYearPermitCount.SecondCountry.Alpha2
                                 + "-" + request.CountryCoupleYearPermitCount.Year.ToString()
                                 + "-" + i.ToString("D6");
                var newPermit = new Entities.Permit
                {
                    CountryCoupleYearPermitCountId = request.CountryCoupleYearPermitCount.Id,
                    Used = false,
                    PermitCode = permitCode
                };
                permitList.Add(newPermit);
            }
            await _dbContext.Permits.AddRangeAsync(permitList, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}