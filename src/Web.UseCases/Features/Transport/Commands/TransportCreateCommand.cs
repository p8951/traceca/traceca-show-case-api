﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using Web.UseCases.Features.Transport.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Transport.Commands;

public class TransportCreateCommand: CreateCommand<TransportCreateDto, int>
{
    public class Handler: CreateCommandHandler<TransportCreateCommand, Entities.Transport, TransportCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        
    }
}