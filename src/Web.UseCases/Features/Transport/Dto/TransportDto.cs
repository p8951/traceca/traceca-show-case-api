﻿using Utils.Json;
using Web.UseCases.Features.Auth.User.Dto;
using Web.UseCases.Features.Carrier.Dto;

namespace Web.UseCases.Features.Transport.Dto;

public class TransportDto
{
    public int Id{ get; set; }
    public int CarrierId{ get; set; }
    public CarrierDto Carrier{ get; set; }
    public string GovernmentNumber{ get; set; }
    public string Driver{ get; set; }
    public string Model{ get; set; }
    public UserDto User{ get; set; }
    public List<AdditionalFieldValueJson> AdditionalFieldValues { get; set; }
}