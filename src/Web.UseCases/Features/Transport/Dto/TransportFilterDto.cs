﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.Transport.Dto;

public class TransportFilterDto: BasePagedQuery
{
    public int? CarrierId { get; set; }
}