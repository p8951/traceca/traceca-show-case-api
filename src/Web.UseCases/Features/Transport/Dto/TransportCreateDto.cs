﻿using Utils.Json;

namespace Web.UseCases.Features.Transport.Dto;

public class TransportCreateDto
{
    public int CarrierId{ get; set; }
    public string GovernmentNumber{ get; set; }
    public string Driver{ get; set; }
    public string Model{ get; set; }
    public int UserId{ get; set; }
    public List<AdditionalFieldValueJson>? AdditionalFieldValues { get; set; }
}