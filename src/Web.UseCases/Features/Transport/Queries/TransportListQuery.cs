﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Transport.Queries;

public class TransportListQuery: GetListQuery<TransportFilterDto, TransportDto>
{
    public class Handler: GetListQueryHandler<TransportListQuery, TransportFilterDto, Entities.Transport, TransportDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
    }
}