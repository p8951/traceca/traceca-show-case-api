﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.CarrierPermitCount.Dto;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.Transport.Dto;
using Web.UseCases.Features.TransportPermit.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.Transport.Queries;

public class TransportPagedListQuery: GetPagedListQuery<TransportFilterDto, TransportDto>
{
    public class Handler: GetPagedListQueryHandler<TransportPagedListQuery, TransportFilterDto, Entities.Transport,
        TransportDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.Transport> QueryData(TransportFilterDto filter, IQueryable<Entities.Transport> query)
        {
            query = query
                .Where(x => filter.CarrierId == null || x.CarrierId == filter.CarrierId);
            return base.QueryData(filter, query);
        }
    }
}