﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Paged;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCouplePermitCount.Queries;

public class CountryCouplePermitCountPagedListQuery: GetPagedListQuery<CountryCouplePermitCountFilterDto, CountryCouplePermitCountDto>
{
    public class Handler: GetPagedListQueryHandler<CountryCouplePermitCountPagedListQuery, CountryCouplePermitCountFilterDto, Entities.CountryCouplePermitCount,
        CountryCouplePermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.CountryCouplePermitCount> QueryData(CountryCouplePermitCountFilterDto filter, IQueryable<Entities.CountryCouplePermitCount> query)
        {
            query = query
                .Include(c => c.CountryCoupleYearPermitCount)    
                .Where(x => (filter.SecondCountryId == null
                            || x.CountryCoupleYearPermitCount.SecondCountryId == filter.SecondCountryId)
                && (filter.FirstCountryId == null
                    || x.CountryCoupleYearPermitCount.FirstCountryId == filter.FirstCountryId)
                && (filter.Year == null
                    || x.CountryCoupleYearPermitCount.Year == filter.Year));
            return base.QueryData(filter, query);
        }
    }
}