﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Queries.GetList;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCouplePermitCount.Queries;

public class CountryCouplePermitCountListQuery: GetListQuery<CountryCouplePermitCountFilterDto, CountryCouplePermitCountDto>
{
    
    public class Handler: GetListQueryHandler<CountryCouplePermitCountListQuery, CountryCouplePermitCountFilterDto, Entities.CountryCouplePermitCount, CountryCouplePermitCountDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.CountryCouplePermitCount> QueryData(CountryCouplePermitCountFilterDto filter, IQueryable<Entities.CountryCouplePermitCount> query)
        {
            query = query.Include(x => x.CountryCoupleYearPermitCount).ThenInclude(c => c.FirstCountry)
                .Where(x => filter.Year == null || x.CountryCoupleYearPermitCount.Year == filter.Year)
                .Where(x => filter.SecondCountryId == null ||
                            x.CountryCoupleYearPermitCount.SecondCountryId == filter.SecondCountryId);
            return base.QueryData(filter, query);
        }
    }
}