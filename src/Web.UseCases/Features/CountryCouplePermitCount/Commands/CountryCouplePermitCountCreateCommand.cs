﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCouplePermitCount.Commands;

public class CountryCouplePermitCountCreateCommand: CreateCommand<CountryCouplePermitCountCreateDto, int>
{
    public class Handler: CreateCommandHandler<CountryCouplePermitCountCreateCommand, Entities.CountryCouplePermitCount, CountryCouplePermitCountCreateDto, int>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<int> Handle(CountryCouplePermitCountCreateCommand request, CancellationToken cancellationToken)
        {
            var data = await _dbContext.CountryCouplePermitCounts
                .SingleOrDefaultAsync(
                    x => x.CountryCoupleYearPermitCountId == request.Dto.CountryCoupleYearPermitCountId,
                    cancellationToken);
            if (data != null)
            {
                throw new AppException("Already exists");
            }
            return await base.Handle(request, cancellationToken);
        }
    }
}