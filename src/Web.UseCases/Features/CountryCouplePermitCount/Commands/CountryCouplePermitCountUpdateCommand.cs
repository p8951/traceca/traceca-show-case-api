﻿using AutoMapper;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Utils.Exceptions;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.CountryCouplePermitCount.Dto;
using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.CountryCouplePermitCount.Commands;

public class CountryCouplePermitCountUpdateCommand: IRequest<Unit>
{
    public int Id { get; set; }
    public CountryCouplePermitCountAddCountDto Dto { get; set; }
    public class Handler: BaseHandler<CountryCouplePermitCountUpdateCommand, Unit>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        public override async Task<Unit> Handle(CountryCouplePermitCountUpdateCommand request, CancellationToken cancellationToken)
        {
            var data = await _dbContext.CountryCouplePermitCounts
                .SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (data == null)
            {
                throw new KeyNotFoundException("Not found");
            }

            data.PermitCount += request.Dto.PermitCount;
            _dbContext.CountryCouplePermitCounts.Update(data);
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}