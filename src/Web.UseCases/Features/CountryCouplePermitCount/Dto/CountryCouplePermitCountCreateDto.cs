﻿namespace Web.UseCases.Features.CountryCouplePermitCount.Dto;

public class CountryCouplePermitCountCreateDto
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public int PermitCount { get; set; }
}