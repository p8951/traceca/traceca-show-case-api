﻿namespace Web.UseCases.Features.CountryCouplePermitCount.Dto;

public class CountryCouplePermitCountAddCountDto
{
    public int PermitCount { get; set; }
}