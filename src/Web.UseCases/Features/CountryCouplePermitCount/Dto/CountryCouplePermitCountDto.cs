﻿using Web.UseCases.Features.CountryCoupleYearPermitCount.Dto;
using Web.UseCases.Features.Dictionary.Country.Dto;

namespace Web.UseCases.Features.CountryCouplePermitCount.Dto;

public class CountryCouplePermitCountDto
{
    public int Id { get; set; }
    public int CountryCoupleYearPermitCountId { get; set; } 
    public CountryCoupleYearPermitCountDto CountryCoupleYearPermitCount { get; set; } 
    
    public int PermitCount { get; set; }
}