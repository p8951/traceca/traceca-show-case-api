﻿using Web.UseCases.Features.Common.Paged;

namespace Web.UseCases.Features.CountryCouplePermitCount.Dto;

public class CountryCouplePermitCountFilterDto: BasePagedQuery
{
    public short? Year { get; set; }
    public int? SecondCountryId { get; set; }
    public int? FirstCountryId { get; set; }
}