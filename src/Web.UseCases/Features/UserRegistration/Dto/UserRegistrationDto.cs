﻿using Entities.Dictionary;
using Utils.Enums;

namespace Web.UseCases.Features.UserRegistration.Dto;

public class UserRegistrationDto
{
    public Guid Id { get; set; }
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Patronymic { get; set; }
    public virtual string FullName => LastName + ' ' + FirstName + ' ' + Patronymic;

    public string Username { get; set; }
    public string Email { get; set; }
    public bool Activated { get; set; }
    public RoleEnum Role { get; set; }
    public int? CountryId { get; set; }
    public Country Country { get; set; }

    public string Organization { get; set; }
    public string Contacts { get; set; }
    public DateTime CreatedAt { get; set; }
}