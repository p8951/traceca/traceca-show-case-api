﻿using Utils.Enums;

namespace Web.UseCases.Features.UserRegistration.Dto;

public class UserRegistrationCreateDto
{
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Patronymic { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public RoleEnum Role { get; set; }
    public int CountryId { get; set; }
    public string? Organization { get; set; }
    public string Contacts { get; set; }
    public string? Address { get; set; }
    public string? City { get; set; }
}