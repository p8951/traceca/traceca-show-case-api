﻿using AutoMapper;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Web.UseCases.Features.Common.Paged;
using Web.UseCases.Features.Common.Queries.GetPagedList;
using Web.UseCases.Features.UserRegistration.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.UserRegistration.Queries;

public class UserRegistrationPagedListQuery: GetPagedListQuery<UserRegistrationFilterDto, UserRegistrationDto>
{
    public class Handler: GetPagedListQueryHandler<UserRegistrationPagedListQuery, UserRegistrationFilterDto, Entities.UserRegistration, UserRegistrationDto, Guid>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }

        protected override IQueryable<Entities.UserRegistration> QueryData(UserRegistrationFilterDto filter, IQueryable<Entities.UserRegistration> query)
        {
            query = query.Include(x => x.Country);
            return base.QueryData(filter, query);
        }
    }
}