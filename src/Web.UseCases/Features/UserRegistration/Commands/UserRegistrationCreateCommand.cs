﻿using AutoMapper;
using DataAccess.Interfaces;
using Web.UseCases.Features.Common.Commands.Create;
using Web.UseCases.Features.UserRegistration.Dto;
using WebApp.Interfaces;

namespace Web.UseCases.Features.UserRegistration.Commands;

public class UserRegistrationCreateCommand: CreateCommand<UserRegistrationCreateDto, Guid>
{
    public class Handler: CreateCommandHandler<UserRegistrationCreateCommand, Entities.UserRegistration, UserRegistrationCreateDto, Guid>
    {
        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService) : base(mapper, dbContext, httpContextService)
        {
        }
        
    }
}