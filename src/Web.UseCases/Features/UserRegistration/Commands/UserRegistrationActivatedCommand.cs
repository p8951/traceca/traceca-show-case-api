﻿using Auth.Interfaces;
using AutoMapper;
using DataAccess.Interfaces;
using Entities.Auth;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Utils;
using Utils.Enums;
using Utils.Exceptions;
using Web.UseCases.Features.Common;
using Web.UseCases.Features.Common.Commands.Update;
using WebApp.Interfaces;

namespace Web.UseCases.Features.UserRegistration.Commands;

public class UserRegistrationActivatedCommand : IRequest<Unit>
{
    public Guid Id { get; set; }

    public class Handler : BaseHandler<UserRegistrationActivatedCommand, Unit>
    {
        private readonly ISender _sender;
        private readonly JwtAppSettings _jwtAppSettings;

        public Handler(IMapper mapper, IDbContext dbContext, IHttpContextService httpContextService,
            ISender sender, IOptions<JwtAppSettings> jwtAppSettings) : base(mapper, dbContext, httpContextService)
        {
            _sender = sender;
            _jwtAppSettings = jwtAppSettings.Value;
        }

        public override async Task<Unit> Handle(UserRegistrationActivatedCommand request,
            CancellationToken cancellationToken)
        {
            var userRegistration =
                await _dbContext.UserRegistrations.SingleOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            if (userRegistration == null)
            {
                throw new KeyNotFoundException();
            }

            if (userRegistration.Activated)
            {
                throw new AppException("Registration already activated");
            }

            userRegistration.Activated = true;
            userRegistration.ActivatedDateTime = DateTime.UtcNow; 
            _dbContext.UserRegistrations.Update(userRegistration);

            var user = await _dbContext.Users.Where(x => x.Username == userRegistration.Username)
                .SingleOrDefaultAsync(cancellationToken);
            if (user != null)
            {
                user.IsActive = true;
                _dbContext.Users.Update(user);
            }
            else
            {
                var searchRole = RoleEnum.Traceca;
                if (userRegistration.Role == RoleEnum.Carrier)
                {
                    searchRole = RoleEnum.Carrier;
                }
                var role = await _dbContext.Roles.SingleAsync(x => x.Id == searchRole, cancellationToken);
                var newUserRoles = new List<Role>();
                newUserRoles.Add(role);
                user = new User
                {
                    PasswordHash = BCrypt.Net.BCrypt.HashPassword(userRegistration.Password, _jwtAppSettings.Salt),
                    LastName = userRegistration.LastName,
                    FirstName = userRegistration.FirstName,
                    Patronymic = userRegistration.Patronymic,
                    Username = userRegistration.Username,
                    CountryId = userRegistration.CountryId.Value,
                    Organization = userRegistration.Organization,
                    Email = userRegistration.Email,
                    IsActive = true,
                    Contacts = userRegistration.Contacts,
                    Roles = newUserRoles
                };
                await _dbContext.Users.AddAsync(user, cancellationToken);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);

            if (userRegistration.Role == RoleEnum.Carrier)
            {
                var newCarrier = new Entities.Carrier
                {
                    CountryId = userRegistration.CountryId.Value,
                    Name = new TranslationNameJson {En = userRegistration.Organization, Ru = ""},
                    Contacts = userRegistration.Contacts,
                    Organization = userRegistration.Organization,
                    Address = userRegistration.Address,
                    City = userRegistration.City,
                    Email = userRegistration.Email
                };
                await _dbContext.Carriers.AddAsync(newCarrier, cancellationToken);
                await _dbContext.SaveChangesAsync(cancellationToken);

                var userData = await _dbContext.Users.Where(x => x.Id == user.Id).SingleOrDefaultAsync(cancellationToken);
                if (userData != null)
                {
                    userData.CarrierId = newCarrier.Id;
                    _dbContext.Users.Update(userData);
                    await _dbContext.SaveChangesAsync(cancellationToken);
                }
                
            }
            
            return Unit.Value;
        }
    }
}