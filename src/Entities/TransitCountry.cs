﻿using Entities.Common;
using Entities.Dictionary;

namespace Entities;

public class TransitCountry: BaseEntity<int>
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public CountryCoupleYearPermitCount CountryCoupleYearPermitCount { get; set; }
    public int CountryId { get; set; }
    public Country Country { get; set; }
}