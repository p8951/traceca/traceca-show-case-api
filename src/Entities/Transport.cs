﻿using Entities.Auth;
using Entities.Common;
using Utils.Json;

namespace Entities;

public class Transport: BaseEntity<int>
{
    public int CarrierId{ get; set; }
    public Carrier Carrier{ get; set; }
    public string GovernmentNumber{ get; set; }
    public string Driver{ get; set; }
    public string Model{ get; set; }
    public int? UserId{ get; set; }
    public User User { get; set; }
    public List<AdditionalFieldValueJson> AdditionalFieldValues { get; set; }
}