﻿using Entities.Common;

namespace Entities;

public class TransportPermit: BaseEntity<int>
{
    public int TransportId { get; set; }
    public Transport Transport { get; set; }
    public long PermitId { get; set; }
    public Permit Permit { get; set; }
    public bool Used { get; set; } = false;
}