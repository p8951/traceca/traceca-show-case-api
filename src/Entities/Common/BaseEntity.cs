﻿using System.ComponentModel.DataAnnotations;

namespace Entities.Common;

public abstract class BaseEntity<T> : BaseEntityField
{
    [Key]
    public T Id { get; set; }
}