﻿using System.ComponentModel.DataAnnotations;

namespace Entities.Common;

public abstract class BaseEntityField
{
    /// <summary>
    /// Дата создания сущности
    /// </summary>
    [Required]
    public DateTime CreatedAt { get; set; }
        
    /// <summary>
    /// Дата последнего изменения сущности
    /// </summary>
    [Required]
    public DateTime UpdatedAt { get; set; }
}