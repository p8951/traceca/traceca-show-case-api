﻿using Entities.Common;
using Utils.Enums;

namespace Entities;

public class Permit: BaseEntity<long>
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public CountryCoupleYearPermitCount CountryCoupleYearPermitCount { get; set; }
    public string PermitCode { get; set; }
    public bool Used { get; set; } = false;
}