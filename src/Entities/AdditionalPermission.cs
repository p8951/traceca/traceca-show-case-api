﻿using Entities.Common;
using Utils.Json;

namespace Entities;

public class AdditionalPermission: BaseEntity<int>
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public CountryCoupleYearPermitCount CountryCoupleYearPermitCount { get; set; }
    public List<AdditionalFieldJson> Fields { get; set; }
}