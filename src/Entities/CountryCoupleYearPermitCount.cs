﻿using Entities.Common;
using Entities.Dictionary;

namespace Entities;

public class CountryCoupleYearPermitCount: BaseEntity<int>
{
    public int FirstCountryId { get; set; }
    public Country FirstCountry { get; set; }
    public int SecondCountryId { get; set; }
    public Country SecondCountry { get; set; }
    public short Year { get; set; }
    public int CountPermit { get; set; }
    public List<TransitCountry> TransitCountries { get; set; }
}