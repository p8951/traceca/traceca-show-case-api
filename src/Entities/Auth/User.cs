﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Entities.Common;
using Entities.Dictionary;

namespace Entities.Auth;

public class User : BaseEntity<int>
{
    [StringLength(200)] 
    public string LastName { get; set; }
    [StringLength(200)] 
    public string FirstName { get; set; }
    [StringLength(200)] 
    public string Patronymic { get; set; }
    public virtual string FullName => LastName + ' ' + FirstName + ' ' + Patronymic;

    public string PasswordHash { get; set; }
    [StringLength(100)] 
    public string Username { get; set; }
    [StringLength(200)] 
    public string Email { get; set; }
    public bool IsActive { get; set; } = true;
    public List<Role> Roles { get; set; }
    public List<RefreshToken> RefreshTokens { get; set; }
    public int CountryId { get; set; }
    public Country? Country { get; set; }

    public string Organization { get; set; }
    public string Contacts { get; set; }
    public int? CarrierId { get; set; }
}