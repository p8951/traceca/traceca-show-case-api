﻿using Utils.Enums;

namespace Entities.Auth;

public class UserRole
{
    public int UserId { get; set; }
    public RoleEnum RoleId { get; set; }
}