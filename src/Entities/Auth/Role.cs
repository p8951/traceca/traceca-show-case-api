﻿using System.ComponentModel.DataAnnotations;
using Entities.Common;
using Utils.Enums;

namespace Entities.Auth;

public class Role: BaseEntity<RoleEnum>
{
    public string Code { get; set; }
    public string Name { get; set; }
    public List<User> Users { get; set; }
}