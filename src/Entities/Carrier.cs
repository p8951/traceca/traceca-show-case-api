﻿using Entities.Common;
using Entities.Dictionary;
using Utils;
using Utils.Json;

namespace Entities;

public class Carrier: BaseEntity<int>
{
    public TranslationNameJson Name { get; set; }
    public int CountryId { get; set; }
    public Country Country { get; set; }
    public string Contacts { get; set; }
    public string Organization { get; set; }
    public string Email { get; set; }
    public string City { get; set; }
    public string Address { get; set; }
    public List<AdditionalFieldValueJson> AdditionalFieldValues { get; set; }
}