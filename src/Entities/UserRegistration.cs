﻿using System.ComponentModel.DataAnnotations;
using Entities.Common;
using Entities.Dictionary;
using Utils.Enums;

namespace Entities;

public class UserRegistration: BaseEntity<Guid>
{
    [StringLength(200)] 
    public string LastName { get; set; }
    [StringLength(200)] 
    public string FirstName { get; set; }
    [StringLength(200)] 
    public string Patronymic { get; set; }
    public virtual string FullName => LastName + ' ' + FirstName + ' ' + Patronymic;

    [StringLength(100)] 
    public string Username { get; set; }
    [StringLength(200)] 
    public string Email { get; set; }
    public bool Activated { get; set; } = false;
    public DateTime? ActivatedDateTime { get; set; }
    public RoleEnum Role { get; set; }
    public int? CountryId { get; set; }
    public Country Country { get; set; }

    public string Organization { get; set; }
    public string Address { get; set; }
    public string City { get; set; }
    public string Contacts { get; set; }
    public string Password { get; set; }
}