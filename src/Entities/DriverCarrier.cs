﻿using Entities.Auth;
using Entities.Common;

namespace Entities;

public class DriverCarrier: BaseEntity<long>
{
    public int UserId { get; set; }
    public User User { get; set; }
    public int CarrierId { get; set; }
    public Carrier Carrier { get; set; }
}