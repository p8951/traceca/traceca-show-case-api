﻿using Entities.Common;

namespace Entities;

public class CountryCouplePermitCount: BaseEntity<int>
{
    public int CountryCoupleYearPermitCountId { get; set; }
    public CountryCoupleYearPermitCount CountryCoupleYearPermitCount { get; set; }
    public int PermitCount { get; set; }
}