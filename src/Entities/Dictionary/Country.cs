﻿using Entities.Common;
using Utils;

namespace Entities.Dictionary;

public class Country: BaseEntity<int>
{
    public TranslationNameJson Name { get; set; } = new TranslationNameJson();
    public string Alpha2 { get; set; }
    public string Alpha3 { get; set; }
    public short Iso { get; set; }
}