﻿using Entities.Common;

namespace Entities;

public class CarrierPermitCount: BaseEntity<int>
{
    public int CarrierId { get; set; }
    public Carrier Carrier { get; set; }
    public int CountryCouplePermitCountId { get; set; }
    public CountryCouplePermitCount CountryCouplePermitCount { get; set; }
    public int PermitCount { get; set; }
}