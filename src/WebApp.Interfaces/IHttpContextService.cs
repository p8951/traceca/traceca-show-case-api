﻿using System;

namespace WebApp.Interfaces
{
    public interface IHttpContextService
    {
        int UserId { get; }
    }
}