﻿using System.Reflection;
using DataAccess.Interfaces;
using DataAccess.PostgreSql.Configs;
using Entities;
using Entities.Auth;
using Entities.Common;
using Entities.Dictionary;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.PostgreSql;

public class AppDbContext: DbContext, IDbContext
{
    public DbSet<Role> Roles { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<Country> Countries { get; set; }
    public DbSet<UserRegistration> UserRegistrations { get; set; }
    public DbSet<Carrier> Carriers { get; set; }
    public DbSet<CarrierPermitCount> CarrierPermitCounts { get; set; }
    public DbSet<Transport> Transports { get; set; }
    public DbSet<TransportPermit> TransportPermits { get; set; }
    public DbSet<CountryCoupleYearPermitCount> CountryCoupleYearPermitCounts { get; set; }
    public DbSet<CountryCouplePermitCount> CountryCouplePermitCounts { get; set; }
    public DbSet<Permit> Permits { get; set; }
    public DbSet<TransitCountry> TransitCountries { get; set; }
    public DbSet<AdditionalPermission> AdditionalPermissions { get; set; }
    public DbSet<DriverCarrier> DriverCarriers { get; set; }
    public DbSet<UserRole> UserRoles { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        base.OnModelCreating(modelBuilder);
    }
    
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        var dt = DateTime.UtcNow;
        foreach (var entry in ChangeTracker.Entries<BaseEntityField>())
        {
            switch (entry.State)
            {
                case EntityState.Added:
                    entry.Entity.CreatedAt = dt;
                    entry.Entity.UpdatedAt = dt;
                    break;
                case EntityState.Modified:
                    entry.Entity.UpdatedAt = dt;
                    break;
            }
        }
        return base.SaveChangesAsync(cancellationToken);
    }
}