﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class CountryCoupleYearPermitCountConfig: IEntityTypeConfiguration<CountryCoupleYearPermitCount>
{
    public void Configure(EntityTypeBuilder<CountryCoupleYearPermitCount> builder)
    {
        builder.HasOne(x => x.FirstCountry)
            .WithMany().HasForeignKey(c => c.FirstCountryId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.SecondCountry)
            .WithMany().HasForeignKey(c => c.SecondCountryId).IsRequired().OnDelete(DeleteBehavior.Restrict);

    }
}