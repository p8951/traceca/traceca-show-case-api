﻿using Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Utils.Enums;

namespace DataAccess.PostgreSql.Configs;

public class RoleConfig: IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasIndex(x => x.Code).IsUnique();
        builder.Property(c => c.Id).ValueGeneratedNever();
        builder.HasData(new Role
            {
                Id = RoleEnum.Admin,
                Code = "admin",
                Name = "Administrator",
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            },
            new Role
            {
                Id = RoleEnum.Traceca,
                Code = "user",
                Name = "User",
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            }
        );
    }
}