﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using Utils.Json;

namespace DataAccess.PostgreSql.Configs;

public class AdditionalPermissionConfig: IEntityTypeConfiguration<AdditionalPermission>
{
    public void Configure(EntityTypeBuilder<AdditionalPermission> builder)
    {
        builder.Property(x => x.CountryCoupleYearPermitCountId).IsRequired();
        builder.HasIndex(x => x.CountryCoupleYearPermitCountId).IsUnique();
        builder.HasOne(x => x.CountryCoupleYearPermitCount)
            .WithMany().HasForeignKey(c => c.CountryCoupleYearPermitCountId).IsRequired().OnDelete(DeleteBehavior.Restrict);

        builder
            .Property(b => b.Fields)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v => JsonConvert.DeserializeObject<List<AdditionalFieldJson>>(v) ?? new List<AdditionalFieldJson>());
    }
}