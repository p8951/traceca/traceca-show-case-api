﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class CountryCouplePermitCountConfig: IEntityTypeConfiguration<CountryCouplePermitCount>
{
    public void Configure(EntityTypeBuilder<CountryCouplePermitCount> builder)
    {
        builder.HasOne(x => x.CountryCoupleYearPermitCount)
            .WithMany().HasForeignKey(c => c.CountryCoupleYearPermitCountId).IsRequired().OnDelete(DeleteBehavior.Restrict);
    }
}