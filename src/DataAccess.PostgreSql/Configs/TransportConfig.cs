﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using Utils.Json;

namespace DataAccess.PostgreSql.Configs;

public class TransportConfig: IEntityTypeConfiguration<Transport>
{
    public void Configure(EntityTypeBuilder<Transport> builder)
    {
        builder.HasOne(x => x.Carrier)
            .WithMany().HasForeignKey(c => c.CarrierId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.User)
            .WithMany().HasForeignKey(c => c.UserId).OnDelete(DeleteBehavior.Restrict);
        builder
            .Property(b => b.AdditionalFieldValues).IsRequired(false);
        builder
            .Property(b => b.AdditionalFieldValues)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v =>  JsonConvert.DeserializeObject<List<AdditionalFieldValueJson>>(v) ?? new List<AdditionalFieldValueJson>());
    }
}