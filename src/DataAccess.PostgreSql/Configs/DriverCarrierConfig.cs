﻿using Entities;
using Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class DriverCarrierConfig: IEntityTypeConfiguration<DriverCarrier>
{
    public void Configure(EntityTypeBuilder<DriverCarrier> builder)
    {
        builder.HasOne(x => x.User)
            .WithMany().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.Carrier)
            .WithMany().HasForeignKey(c => c.CarrierId).IsRequired().OnDelete(DeleteBehavior.Restrict);
    }
}