﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using Utils;
using Utils.Json;

namespace DataAccess.PostgreSql.Configs;

public class CarrierConfig: IEntityTypeConfiguration<Carrier>
{
    public void Configure(EntityTypeBuilder<Carrier> builder)
    {
        builder.HasOne(x => x.Country)
            .WithMany().HasForeignKey(c => c.CountryId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder
            .Property(b => b.Name)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v =>  JsonConvert.DeserializeObject<TranslationNameJson>(v) ?? new TranslationNameJson());
        builder
            .Property(b => b.AdditionalFieldValues).IsRequired(false)
            .HasConversion(
                v => JsonConvert.SerializeObject(v),
                v =>  JsonConvert.DeserializeObject<List<AdditionalFieldValueJson>>(v) ?? new List<AdditionalFieldValueJson>());
        builder
            .Property(b => b.Address).IsRequired(false);
        builder
            .Property(b => b.City).IsRequired(false);
    }
}