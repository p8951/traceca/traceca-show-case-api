﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class CarrierPermitCountConfig: IEntityTypeConfiguration<CarrierPermitCount>
{
    public void Configure(EntityTypeBuilder<CarrierPermitCount> builder)
    {
        builder.HasOne(x => x.Carrier)
            .WithMany().HasForeignKey(c => c.CarrierId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.CountryCouplePermitCount)
            .WithMany().HasForeignKey(c => c.CountryCouplePermitCountId).IsRequired().OnDelete(DeleteBehavior.Restrict);
    }
}