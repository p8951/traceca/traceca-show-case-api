﻿using Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Utils.Enums;

namespace DataAccess.PostgreSql.Configs;

public class UserConfig: IEntityTypeConfiguration<User> 
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.HasIndex(x => x.Username).IsUnique();
        builder.HasOne(x => x.Country)
            .WithMany().HasForeignKey(c => c.CountryId).OnDelete(DeleteBehavior.Restrict);            
        builder.OwnsMany(x => x.RefreshTokens);
        builder
            .HasMany(x => x.Roles)
            .WithMany(c => c.Users)
            .UsingEntity<UserRole>(j => j.ToTable("user_roles"));
        builder.Property(c => c.Id)
            .HasIdentityOptions(startValue: 2);
        builder.HasData(
            new User
            {
                Id = 1,
                Email = "e.arbabayev@kmg.kz",
                LastName = "Иванов",
                FirstName = "Иван",
                Patronymic = "Иванович",
                Username = "admin",
                IsActive = true,
                PasswordHash = "$2a$11$i0cRGfSNEq.ezit.c.hoE.FcYNhP15d6PNjVZfQ4OQKySTkr08Y5G",
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow,
                Contacts = "",
                Organization = "",
                CountryId = 1
            }
        );
    }
}