﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class UserRegistrationConfig: IEntityTypeConfiguration<UserRegistration> 
{
    public void Configure(EntityTypeBuilder<UserRegistration> builder)
    {
        builder.HasOne(x => x.Country)
            .WithMany().HasForeignKey(c => c.CountryId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        
        builder.Property(p => p.Address).IsRequired(false);
        builder.Property(p => p.City).IsRequired(false);
        builder.Property(p => p.Organization).IsRequired(false);
    }
}