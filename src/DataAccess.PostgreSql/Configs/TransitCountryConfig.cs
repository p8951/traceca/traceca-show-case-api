﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class TransitCountryConfig: IEntityTypeConfiguration<TransitCountry>
{
    public void Configure(EntityTypeBuilder<TransitCountry> builder)
    {
        builder.Property(x => x.CountryId).IsRequired();
        builder.HasOne(x => x.Country)
            .WithMany().HasForeignKey(c => c.CountryId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.CountryCoupleYearPermitCount)
            .WithMany(v => v.TransitCountries).HasForeignKey(c => c.CountryCoupleYearPermitCountId).IsRequired().OnDelete(DeleteBehavior.Restrict);
    }
}