﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.PostgreSql.Configs;

public class TransportPermitConfig: IEntityTypeConfiguration<TransportPermit>
{
    public void Configure(EntityTypeBuilder<TransportPermit> builder)
    {
        builder.HasOne(x => x.Permit)
            .WithMany().HasForeignKey(c => c.PermitId).IsRequired().OnDelete(DeleteBehavior.Restrict);
        builder.HasOne(x => x.Transport)
            .WithMany().HasForeignKey(c => c.TransportId).IsRequired().OnDelete(DeleteBehavior.Restrict);

    }
}