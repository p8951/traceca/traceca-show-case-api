﻿using Entities.Dictionary;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using Utils;

namespace DataAccess.PostgreSql.Configs.Dictionary;

public class CountryConfig : IEntityTypeConfiguration<Country>
{
    public void Configure(EntityTypeBuilder<Country> builder)
    {
        builder.Property(x => x.Name)
            .HasColumnType("jsonb");
        builder.Property(c => c.Id)
            .HasIdentityOptions(startValue: 7);
        // var dt = DateTime.UtcNow;
        // builder.HasData(
        //     new Country
        //     {
        //         Id = 1, Name = new TranslationNameJson {Ru = "Абхазия", En = "Abkhazia"}, Alpha2 = "AB", Alpha3 = "ABH",
        //         Iso = 895, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 2, Name = new TranslationNameJson {Ru = "Австралия", En = "Australia"}, Alpha2 = "AU",
        //         Alpha3 = "AUS", Iso = 36, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 3, Name = new TranslationNameJson {Ru = "Австрия", En = "Austria"}, Alpha2 = "AT", Alpha3 = "AUT",
        //         Iso = 40, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 4, Name = new TranslationNameJson {Ru = "Азербайджан", En = "Azerbaijan"}, Alpha2 = "AZ",
        //         Alpha3 = "AZE", Iso = 31, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 5, Name = new TranslationNameJson {Ru = "Албания", En = "Albania"}, Alpha2 = "AL", Alpha3 = "ALB",
        //         Iso = 8, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 6, Name = new TranslationNameJson {Ru = "Алжир", En = "Algeria"}, Alpha2 = "DZ", Alpha3 = "DZA",
        //         Iso = 12, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 7, Name = new TranslationNameJson {Ru = "Американское Самоа", En = "American Samoa"},
        //         Alpha2 = "AS", Alpha3 = "ASM", Iso = 16, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 8, Name = new TranslationNameJson {Ru = "Ангилья", En = "Anguilla"}, Alpha2 = "AI", Alpha3 = "AIA",
        //         Iso = 660, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 9, Name = new TranslationNameJson {Ru = "Ангола", En = "Angola"}, Alpha2 = "AO", Alpha3 = "AGO",
        //         Iso = 24, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 10, Name = new TranslationNameJson {Ru = "Андорра", En = "Andorra"}, Alpha2 = "AD", Alpha3 = "AND",
        //         Iso = 20, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 11, Name = new TranslationNameJson {Ru = "Антарктида", En = "Antarctica"}, Alpha2 = "AQ",
        //         Alpha3 = "ATA", Iso = 10, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 12, Name = new TranslationNameJson {Ru = "Антигуа и Барбуда", En = "Antigua and Barbuda"},
        //         Alpha2 = "AG", Alpha3 = "ATG", Iso = 28, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 13, Name = new TranslationNameJson {Ru = "Аргентина", En = "Argentina"}, Alpha2 = "AR",
        //         Alpha3 = "ARG", Iso = 32, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 14, Name = new TranslationNameJson {Ru = "Армения", En = "Armenia"}, Alpha2 = "AM", Alpha3 = "ARM",
        //         Iso = 51, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 15, Name = new TranslationNameJson {Ru = "Аруба", En = "Aruba"}, Alpha2 = "AW", Alpha3 = "ABW",
        //         Iso = 533, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 16, Name = new TranslationNameJson {Ru = "Афганистан", En = "Afghanistan"}, Alpha2 = "AF",
        //         Alpha3 = "AFG", Iso = 4, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 17, Name = new TranslationNameJson {Ru = "Багамы", En = "Bahamas"}, Alpha2 = "BS", Alpha3 = "BHS",
        //         Iso = 44, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 18, Name = new TranslationNameJson {Ru = "Бангладеш", En = "Bangladesh"}, Alpha2 = "BD",
        //         Alpha3 = "BGD", Iso = 50, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 19, Name = new TranslationNameJson {Ru = "Барбадос", En = "Barbados"}, Alpha2 = "BB",
        //         Alpha3 = "BRB", Iso = 52, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 20, Name = new TranslationNameJson {Ru = "Бахрейн", En = "Bahrain"}, Alpha2 = "BH", Alpha3 = "BHR",
        //         Iso = 48, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 21, Name = new TranslationNameJson {Ru = "Беларусь", En = "Belarus"}, Alpha2 = "BY",
        //         Alpha3 = "BLR", Iso = 112, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 22, Name = new TranslationNameJson {Ru = "Белиз", En = "Belize"}, Alpha2 = "BZ", Alpha3 = "BLZ",
        //         Iso = 84, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 23, Name = new TranslationNameJson {Ru = "Бельгия", En = "Belgium"}, Alpha2 = "BE", Alpha3 = "BEL",
        //         Iso = 56, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 24, Name = new TranslationNameJson {Ru = "Бенин", En = "Benin"}, Alpha2 = "BJ", Alpha3 = "BEN",
        //         Iso = 204, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 25, Name = new TranslationNameJson {Ru = "Бермуды", En = "Bermuda"}, Alpha2 = "BM", Alpha3 = "BMU",
        //         Iso = 60, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 26, Name = new TranslationNameJson {Ru = "Болгария", En = "Bulgaria"}, Alpha2 = "BG",
        //         Alpha3 = "BGR", Iso = 100, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 27,
        //         Name = new TranslationNameJson
        //             {Ru = "Боливия, Многонациональное Государство", En = "Bolivia, plurinational state of"},
        //         Alpha2 = "BO", Alpha3 = "BOL", Iso = 68, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 28,
        //         Name = new TranslationNameJson
        //             {Ru = "Бонайре, Саба и Синт-Эстатиус", En = "Bonaire, Sint Eustatius and Saba"},
        //         Alpha2 = "BQ", Alpha3 = "BES", Iso = 535, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 29, Name = new TranslationNameJson {Ru = "Босния и Герцеговина", En = "Bosnia and Herzegovina"},
        //         Alpha2 = "BA", Alpha3 = "BIH", Iso = 70, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 30, Name = new TranslationNameJson {Ru = "Ботсвана", En = "Botswana"}, Alpha2 = "BW",
        //         Alpha3 = "BWA", Iso = 72, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 31, Name = new TranslationNameJson {Ru = "Бразилия", En = "Brazil"}, Alpha2 = "BR", Alpha3 = "BRA",
        //         Iso = 76, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 32,
        //         Name = new TranslationNameJson
        //             {Ru = "Британская территория в Индийском океане", En = "British Indian Ocean Territory"},
        //         Alpha2 = "IO", Alpha3 = "IOT", Iso = 86, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 33, Name = new TranslationNameJson {Ru = "Бруней-Даруссалам", En = "Brunei Darussalam"},
        //         Alpha2 = "BN", Alpha3 = "BRN", Iso = 96, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 34, Name = new TranslationNameJson {Ru = "Буркина-Фасо", En = "Burkina Faso"}, Alpha2 = "BF",
        //         Alpha3 = "BFA", Iso = 854, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 35, Name = new TranslationNameJson {Ru = "Бурунди", En = "Burundi"}, Alpha2 = "BI", Alpha3 = "BDI",
        //         Iso = 108, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 36, Name = new TranslationNameJson {Ru = "Бутан", En = "Bhutan"}, Alpha2 = "BT", Alpha3 = "BTN",
        //         Iso = 64, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 37, Name = new TranslationNameJson {Ru = "Вануату", En = "Vanuatu"}, Alpha2 = "VU", Alpha3 = "VUT",
        //         Iso = 548, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 38, Name = new TranslationNameJson {Ru = "Венгрия", En = "Hungary"}, Alpha2 = "HU", Alpha3 = "HUN",
        //         Iso = 348, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 39, Name = new TranslationNameJson {Ru = "Венесуэла Боливарианская Республика", En = "Venezuela"},
        //         Alpha2 = "VE", Alpha3 = "VEN", Iso = 862, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 40,
        //         Name = new TranslationNameJson {Ru = "Виргинские острова, Британские", En = "Virgin Islands, British"},
        //         Alpha2 = "VG", Alpha3 = "VGB", Iso = 92, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 41, Name = new TranslationNameJson {Ru = "Виргинские острова, США", En = "Virgin Islands, U.S."},
        //         Alpha2 = "VI", Alpha3 = "VIR", Iso = 850, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 42, Name = new TranslationNameJson {Ru = "Вьетнам", En = "Vietnam"}, Alpha2 = "VN", Alpha3 = "VNM",
        //         Iso = 704, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 43, Name = new TranslationNameJson {Ru = "Габон", En = "Gabon"}, Alpha2 = "GA", Alpha3 = "GAB",
        //         Iso = 266, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 44, Name = new TranslationNameJson {Ru = "Гаити", En = "Haiti"}, Alpha2 = "HT", Alpha3 = "HTI",
        //         Iso = 332, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 45, Name = new TranslationNameJson {Ru = "Гайана", En = "Guyana"}, Alpha2 = "GY", Alpha3 = "GUY",
        //         Iso = 328, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 46, Name = new TranslationNameJson {Ru = "Гамбия", En = "Gambia"}, Alpha2 = "GM", Alpha3 = "GMB",
        //         Iso = 270, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 47, Name = new TranslationNameJson {Ru = "Гана", En = "Ghana"}, Alpha2 = "GH", Alpha3 = "GHA",
        //         Iso = 288, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 48, Name = new TranslationNameJson {Ru = "Гваделупа", En = "Guadeloupe"}, Alpha2 = "GP",
        //         Alpha3 = "GLP", Iso = 312, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 49, Name = new TranslationNameJson {Ru = "Гватемала", En = "Guatemala"}, Alpha2 = "GT",
        //         Alpha3 = "GTM", Iso = 320, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 50, Name = new TranslationNameJson {Ru = "Гвинея", En = "Guinea"}, Alpha2 = "GN", Alpha3 = "GIN",
        //         Iso = 324, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 51, Name = new TranslationNameJson {Ru = "Гвинея-Бисау", En = "Guinea-Bissau"}, Alpha2 = "GW",
        //         Alpha3 = "GNB", Iso = 624, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 52, Name = new TranslationNameJson {Ru = "Германия", En = "Germany"}, Alpha2 = "DE",
        //         Alpha3 = "DEU", Iso = 276, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 53, Name = new TranslationNameJson {Ru = "Гернси", En = "Guernsey"}, Alpha2 = "GG", Alpha3 = "GGY",
        //         Iso = 831, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 54, Name = new TranslationNameJson {Ru = "Гибралтар", En = "Gibraltar"}, Alpha2 = "GI",
        //         Alpha3 = "GIB", Iso = 292, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 55, Name = new TranslationNameJson {Ru = "Гондурас", En = "Honduras"}, Alpha2 = "HN",
        //         Alpha3 = "HND", Iso = 340, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 56, Name = new TranslationNameJson {Ru = "Гонконг", En = "Hong Kong"}, Alpha2 = "HK",
        //         Alpha3 = "HKG", Iso = 344, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 57, Name = new TranslationNameJson {Ru = "Гренада", En = "Grenada"}, Alpha2 = "GD", Alpha3 = "GRD",
        //         Iso = 308, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 58, Name = new TranslationNameJson {Ru = "Гренландия", En = "Greenland"}, Alpha2 = "GL",
        //         Alpha3 = "GRL", Iso = 304, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 59, Name = new TranslationNameJson {Ru = "Греция", En = "Greece"}, Alpha2 = "GR", Alpha3 = "GRC",
        //         Iso = 300, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 60, Name = new TranslationNameJson {Ru = "Грузия", En = "Georgia"}, Alpha2 = "GE", Alpha3 = "GEO",
        //         Iso = 268, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 61, Name = new TranslationNameJson {Ru = "Гуам", En = "Guam"}, Alpha2 = "GU", Alpha3 = "GUM",
        //         Iso = 316, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 62, Name = new TranslationNameJson {Ru = "Дания", En = "Denmark"}, Alpha2 = "DK", Alpha3 = "DNK",
        //         Iso = 208, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 63, Name = new TranslationNameJson {Ru = "Джерси", En = "Jersey"}, Alpha2 = "JE", Alpha3 = "JEY",
        //         Iso = 832, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 64, Name = new TranslationNameJson {Ru = "Джибути", En = "Djibouti"}, Alpha2 = "DJ",
        //         Alpha3 = "DJI", Iso = 262, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 65, Name = new TranslationNameJson {Ru = "Доминика", En = "Dominica"}, Alpha2 = "DM",
        //         Alpha3 = "DMA", Iso = 212, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 66, Name = new TranslationNameJson {Ru = "Доминиканская Республика", En = "Dominican Republic"},
        //         Alpha2 = "DO", Alpha3 = "DOM", Iso = 214, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 67, Name = new TranslationNameJson {Ru = "Египет", En = "Egypt"}, Alpha2 = "EG", Alpha3 = "EGY",
        //         Iso = 818, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 68, Name = new TranslationNameJson {Ru = "Замбия", En = "Zambia"}, Alpha2 = "ZM", Alpha3 = "ZMB",
        //         Iso = 894, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 69, Name = new TranslationNameJson {Ru = "Западная Сахара", En = "Western Sahara"}, Alpha2 = "EH",
        //         Alpha3 = "ESH", Iso = 732, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 70, Name = new TranslationNameJson {Ru = "Зимбабве", En = "Zimbabwe"}, Alpha2 = "ZW",
        //         Alpha3 = "ZWE", Iso = 716, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 71, Name = new TranslationNameJson {Ru = "Израиль", En = "Israel"}, Alpha2 = "IL", Alpha3 = "ISR",
        //         Iso = 376, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 72, Name = new TranslationNameJson {Ru = "Индия", En = "India"}, Alpha2 = "IN", Alpha3 = "IND",
        //         Iso = 356, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 73, Name = new TranslationNameJson {Ru = "Индонезия", En = "Indonesia"}, Alpha2 = "ID",
        //         Alpha3 = "IDN", Iso = 360, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 74, Name = new TranslationNameJson {Ru = "Иордания", En = "Jordan"}, Alpha2 = "JO", Alpha3 = "JOR",
        //         Iso = 400, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 75, Name = new TranslationNameJson {Ru = "Ирак", En = "Iraq"}, Alpha2 = "IQ", Alpha3 = "IRQ",
        //         Iso = 368, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 76,
        //         Name = new TranslationNameJson {Ru = "Иран, Исламская Республика", En = "Iran, Islamic Republic of"},
        //         Alpha2 = "IR", Alpha3 = "IRN", Iso = 364, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 77, Name = new TranslationNameJson {Ru = "Ирландия", En = "Ireland"}, Alpha2 = "IE",
        //         Alpha3 = "IRL", Iso = 372, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 78, Name = new TranslationNameJson {Ru = "Исландия", En = "Iceland"}, Alpha2 = "IS",
        //         Alpha3 = "ISL", Iso = 352, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 79, Name = new TranslationNameJson {Ru = "Испания", En = "Spain"}, Alpha2 = "ES", Alpha3 = "ESP",
        //         Iso = 724, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 80, Name = new TranslationNameJson {Ru = "Италия", En = "Italy"}, Alpha2 = "IT", Alpha3 = "ITA",
        //         Iso = 380, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 81, Name = new TranslationNameJson {Ru = "Йемен", En = "Yemen"}, Alpha2 = "YE", Alpha3 = "YEM",
        //         Iso = 887, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 82, Name = new TranslationNameJson {Ru = "Кабо-Верде", En = "Cape Verde"}, Alpha2 = "CV",
        //         Alpha3 = "CPV", Iso = 132, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 83, Name = new TranslationNameJson {Ru = "Казахстан", En = "Kazakhstan"}, Alpha2 = "KZ",
        //         Alpha3 = "KAZ", Iso = 398, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 84, Name = new TranslationNameJson {Ru = "Камбоджа", En = "Cambodia"}, Alpha2 = "KH",
        //         Alpha3 = "KHM", Iso = 116, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 85, Name = new TranslationNameJson {Ru = "Камерун", En = "Cameroon"}, Alpha2 = "CM",
        //         Alpha3 = "CMR", Iso = 120, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 86, Name = new TranslationNameJson {Ru = "Канада", En = "Canada"}, Alpha2 = "CA", Alpha3 = "CAN",
        //         Iso = 124, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 87, Name = new TranslationNameJson {Ru = "Катар", En = "Qatar"}, Alpha2 = "QA", Alpha3 = "QAT",
        //         Iso = 634, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 88, Name = new TranslationNameJson {Ru = "Кения", En = "Kenya"}, Alpha2 = "KE", Alpha3 = "KEN",
        //         Iso = 404, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 89, Name = new TranslationNameJson {Ru = "Кипр", En = "Cyprus"}, Alpha2 = "CY", Alpha3 = "CYP",
        //         Iso = 196, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 90, Name = new TranslationNameJson {Ru = "Киргизия", En = "Kyrgyzstan"}, Alpha2 = "KG",
        //         Alpha3 = "KGZ", Iso = 417, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 91, Name = new TranslationNameJson {Ru = "Кирибати", En = "Kiribati"}, Alpha2 = "KI",
        //         Alpha3 = "KIR", Iso = 296, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 92, Name = new TranslationNameJson {Ru = "Китай", En = "China"}, Alpha2 = "CN", Alpha3 = "CHN",
        //         Iso = 156, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 93,
        //         Name = new TranslationNameJson {Ru = "Кокосовые (Килинг) острова", En = "Cocos (Keeling) Islands"},
        //         Alpha2 = "CC", Alpha3 = "CCK", Iso = 166, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 94, Name = new TranslationNameJson {Ru = "Колумбия", En = "Colombia"}, Alpha2 = "CO",
        //         Alpha3 = "COL", Iso = 170, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 95, Name = new TranslationNameJson {Ru = "Коморы", En = "Comoros"}, Alpha2 = "KM", Alpha3 = "COM",
        //         Iso = 174, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 96, Name = new TranslationNameJson {Ru = "Конго", En = "Congo"}, Alpha2 = "CG", Alpha3 = "COG",
        //         Iso = 178, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 97,
        //         Name = new TranslationNameJson
        //             {Ru = "Конго, Демократическая Республика", En = "Congo, Democratic Republic of the"},
        //         Alpha2 = "CD", Alpha3 = "COD", Iso = 180, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 98,
        //         Name = new TranslationNameJson
        //             {Ru = "Корея, Народно-Демократическая Республика", En = "Korea, Democratic People's republic of"},
        //         Alpha2 = "KP", Alpha3 = "PRK", Iso = 408, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 99, Name = new TranslationNameJson {Ru = "Корея, Республика", En = "Korea, Republic of"},
        //         Alpha2 = "KR", Alpha3 = "KOR", Iso = 410, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 100, Name = new TranslationNameJson {Ru = "Коста-Рика", En = "Costa Rica"}, Alpha2 = "CR",
        //         Alpha3 = "CRI", Iso = 188, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 101, Name = new TranslationNameJson {Ru = "Кот д'Ивуар", En = "Cote d'Ivoire"}, Alpha2 = "CI",
        //         Alpha3 = "CIV", Iso = 384, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 102, Name = new TranslationNameJson {Ru = "Куба", En = "Cuba"}, Alpha2 = "CU", Alpha3 = "CUB",
        //         Iso = 192, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 103, Name = new TranslationNameJson {Ru = "Кувейт", En = "Kuwait"}, Alpha2 = "KW", Alpha3 = "KWT",
        //         Iso = 414, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 104, Name = new TranslationNameJson {Ru = "Кюрасао", En = "Curaçao"}, Alpha2 = "CW",
        //         Alpha3 = "CUW", Iso = 531, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 105, Name = new TranslationNameJson {Ru = "Лаос", En = "Lao People's Democratic Republic"},
        //         Alpha2 = "LA", Alpha3 = "LAO", Iso = 418, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 106, Name = new TranslationNameJson {Ru = "Латвия", En = "Latvia"}, Alpha2 = "LV", Alpha3 = "LVA",
        //         Iso = 428, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 107, Name = new TranslationNameJson {Ru = "Лесото", En = "Lesotho"}, Alpha2 = "LS", Alpha3 = "LSO",
        //         Iso = 426, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 108, Name = new TranslationNameJson {Ru = "Ливан", En = "Lebanon"}, Alpha2 = "LB", Alpha3 = "LBN",
        //         Iso = 422, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 109,
        //         Name = new TranslationNameJson {Ru = "Ливийская Арабская Джамахирия", En = "Libyan Arab Jamahiriya"},
        //         Alpha2 = "LY", Alpha3 = "LBY", Iso = 434, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 110, Name = new TranslationNameJson {Ru = "Либерия", En = "Liberia"}, Alpha2 = "LR",
        //         Alpha3 = "LBR", Iso = 430, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 111, Name = new TranslationNameJson {Ru = "Лихтенштейн", En = "Liechtenstein"}, Alpha2 = "LI",
        //         Alpha3 = "LIE", Iso = 438, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 112, Name = new TranslationNameJson {Ru = "Литва", En = "Lithuania"}, Alpha2 = "LT",
        //         Alpha3 = "LTU", Iso = 440, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 113, Name = new TranslationNameJson {Ru = "Люксембург", En = "Luxembourg"}, Alpha2 = "LU",
        //         Alpha3 = "LUX", Iso = 442, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 114, Name = new TranslationNameJson {Ru = "Маврикий", En = "Mauritius"}, Alpha2 = "MU",
        //         Alpha3 = "MUS", Iso = 480, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 115, Name = new TranslationNameJson {Ru = "Мавритания", En = "Mauritania"}, Alpha2 = "MR",
        //         Alpha3 = "MRT", Iso = 478, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 116, Name = new TranslationNameJson {Ru = "Мадагаскар", En = "Madagascar"}, Alpha2 = "MG",
        //         Alpha3 = "MDG", Iso = 450, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 117, Name = new TranslationNameJson {Ru = "Майотта", En = "Mayotte"}, Alpha2 = "YT",
        //         Alpha3 = "MYT", Iso = 175, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 118, Name = new TranslationNameJson {Ru = "Макао", En = "Macao"}, Alpha2 = "MO", Alpha3 = "MAC",
        //         Iso = 446, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 119, Name = new TranslationNameJson {Ru = "Малави", En = "Malawi"}, Alpha2 = "MW", Alpha3 = "MWI",
        //         Iso = 454, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 120, Name = new TranslationNameJson {Ru = "Малайзия", En = "Malaysia"}, Alpha2 = "MY",
        //         Alpha3 = "MYS", Iso = 458, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 121, Name = new TranslationNameJson {Ru = "Мали", En = "Mali"}, Alpha2 = "ML", Alpha3 = "MLI",
        //         Iso = 466, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 122,
        //         Name = new TranslationNameJson
        //         {
        //             Ru = "Малые Тихоокеанские отдаленные острова Соединенных Штатов",
        //             En = "United States Minor Outlying Islands"
        //         },
        //         Alpha2 = "UM", Alpha3 = "UMI", Iso = 581, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 123, Name = new TranslationNameJson {Ru = "Мальдивы", En = "Maldives"}, Alpha2 = "MV",
        //         Alpha3 = "MDV", Iso = 462, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 124, Name = new TranslationNameJson {Ru = "Мальта", En = "Malta"}, Alpha2 = "MT", Alpha3 = "MLT",
        //         Iso = 470, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 125, Name = new TranslationNameJson {Ru = "Марокко", En = "Morocco"}, Alpha2 = "MA",
        //         Alpha3 = "MAR", Iso = 504, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 126, Name = new TranslationNameJson {Ru = "Мартиника", En = "Martinique"}, Alpha2 = "MQ",
        //         Alpha3 = "MTQ", Iso = 474, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 127, Name = new TranslationNameJson {Ru = "Маршалловы острова", En = "Marshall Islands"},
        //         Alpha2 = "MH", Alpha3 = "MHL", Iso = 584, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 128, Name = new TranslationNameJson {Ru = "Мексика", En = "Mexico"}, Alpha2 = "MX", Alpha3 = "MEX",
        //         Iso = 484, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 129,
        //         Name = new TranslationNameJson
        //             {Ru = "Микронезия, Федеративные Штаты", En = "Micronesia, Federated States of"},
        //         Alpha2 = "FM", Alpha3 = "FSM", Iso = 583, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 130, Name = new TranslationNameJson {Ru = "Мозамбик", En = "Mozambique"}, Alpha2 = "MZ",
        //         Alpha3 = "MOZ", Iso = 508, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 131, Name = new TranslationNameJson {Ru = "Молдова, Республика", En = "Moldova"}, Alpha2 = "MD",
        //         Alpha3 = "MDA", Iso = 498, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 132, Name = new TranslationNameJson {Ru = "Монако", En = "Monaco"}, Alpha2 = "MC", Alpha3 = "MCO",
        //         Iso = 492, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 133, Name = new TranslationNameJson {Ru = "Монголия", En = "Mongolia"}, Alpha2 = "MN",
        //         Alpha3 = "MNG", Iso = 496, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 134, Name = new TranslationNameJson {Ru = "Монтсеррат", En = "Montserrat"}, Alpha2 = "MS",
        //         Alpha3 = "MSR", Iso = 500, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 135, Name = new TranslationNameJson {Ru = "Мьянма", En = "Burma"}, Alpha2 = "MM", Alpha3 = "MMR",
        //         Iso = 104, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 136, Name = new TranslationNameJson {Ru = "Намибия", En = "Namibia"}, Alpha2 = "NA",
        //         Alpha3 = "NAM", Iso = 516, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 137, Name = new TranslationNameJson {Ru = "Науру", En = "Nauru"}, Alpha2 = "NR", Alpha3 = "NRU",
        //         Iso = 520, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 138, Name = new TranslationNameJson {Ru = "Непал", En = "Nepal"}, Alpha2 = "NP", Alpha3 = "NPL",
        //         Iso = 524, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 139, Name = new TranslationNameJson {Ru = "Нигер", En = "Niger"}, Alpha2 = "NE", Alpha3 = "NER",
        //         Iso = 562, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 140, Name = new TranslationNameJson {Ru = "Нигерия", En = "Nigeria"}, Alpha2 = "NG",
        //         Alpha3 = "NGA", Iso = 566, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 141, Name = new TranslationNameJson {Ru = "Нидерланды", En = "Netherlands"}, Alpha2 = "NL",
        //         Alpha3 = "NLD", Iso = 528, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 142, Name = new TranslationNameJson {Ru = "Никарагуа", En = "Nicaragua"}, Alpha2 = "NI",
        //         Alpha3 = "NIC", Iso = 558, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 143, Name = new TranslationNameJson {Ru = "Ниуэ", En = "Niue"}, Alpha2 = "NU", Alpha3 = "NIU",
        //         Iso = 570, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 144, Name = new TranslationNameJson {Ru = "Новая Зеландия", En = "New Zealand"}, Alpha2 = "NZ",
        //         Alpha3 = "NZL", Iso = 554, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 145, Name = new TranslationNameJson {Ru = "Новая Каледония", En = "New Caledonia"}, Alpha2 = "NC",
        //         Alpha3 = "NCL", Iso = 540, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 146, Name = new TranslationNameJson {Ru = "Норвегия", En = "Norway"}, Alpha2 = "NO",
        //         Alpha3 = "NOR", Iso = 578, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 147,
        //         Name = new TranslationNameJson {Ru = "Объединенные Арабские Эмираты", En = "United Arab Emirates"},
        //         Alpha2 = "AE", Alpha3 = "ARE", Iso = 784, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 148, Name = new TranslationNameJson {Ru = "Оман", En = "Oman"}, Alpha2 = "OM", Alpha3 = "OMN",
        //         Iso = 512, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 149, Name = new TranslationNameJson {Ru = "Остров Буве", En = "Bouvet Island"}, Alpha2 = "BV",
        //         Alpha3 = "BVT", Iso = 74, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 150, Name = new TranslationNameJson {Ru = "Остров Мэн", En = "Isle of Man"}, Alpha2 = "IM",
        //         Alpha3 = "IMN", Iso = 833, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 151, Name = new TranslationNameJson {Ru = "Остров Норфолк", En = "Norfolk Island"}, Alpha2 = "NF",
        //         Alpha3 = "NFK", Iso = 574, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 152, Name = new TranslationNameJson {Ru = "Остров Рождества", En = "Christmas Island"},
        //         Alpha2 = "CX", Alpha3 = "CXR", Iso = 162, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 153,
        //         Name = new TranslationNameJson
        //             {Ru = "Остров Херд и острова Макдональд", En = "Heard Island and McDonald Islands"},
        //         Alpha2 = "HM", Alpha3 = "HMD", Iso = 334, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 154, Name = new TranslationNameJson {Ru = "Острова Кайман", En = "Cayman Islands"}, Alpha2 = "KY",
        //         Alpha3 = "CYM", Iso = 136, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 155, Name = new TranslationNameJson {Ru = "Острова Кука", En = "Cook Islands"}, Alpha2 = "CK",
        //         Alpha3 = "COK", Iso = 184, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 156,
        //         Name = new TranslationNameJson {Ru = "Острова Теркс и Кайкос", En = "Turks and Caicos Islands"},
        //         Alpha2 = "TC", Alpha3 = "TCA", Iso = 796, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 157, Name = new TranslationNameJson {Ru = "Пакистан", En = "Pakistan"}, Alpha2 = "PK",
        //         Alpha3 = "PAK", Iso = 586, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 158, Name = new TranslationNameJson {Ru = "Палау", En = "Palau"}, Alpha2 = "PW", Alpha3 = "PLW",
        //         Iso = 585, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 159,
        //         Name = new TranslationNameJson
        //             {Ru = "Палестинская территория, оккупированная", En = "Palestinian Territory, Occupied"},
        //         Alpha2 = "PS", Alpha3 = "PSE", Iso = 275, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 160, Name = new TranslationNameJson {Ru = "Панама", En = "Panama"}, Alpha2 = "PA", Alpha3 = "PAN",
        //         Iso = 591, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 161,
        //         Name = new TranslationNameJson
        //             {Ru = "Папский Престол (Государство &mdash; город Ватикан)", En = "Holy See (Vatican City State)"},
        //         Alpha2 = "VA", Alpha3 = "VAT", Iso = 336, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 162, Name = new TranslationNameJson {Ru = "Папуа-Новая Гвинея", En = "Papua New Guinea"},
        //         Alpha2 = "PG", Alpha3 = "PNG", Iso = 598, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 163, Name = new TranslationNameJson {Ru = "Парагвай", En = "Paraguay"}, Alpha2 = "PY",
        //         Alpha3 = "PRY", Iso = 600, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 164, Name = new TranslationNameJson {Ru = "Перу", En = "Peru"}, Alpha2 = "PE", Alpha3 = "PER",
        //         Iso = 604, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 165, Name = new TranslationNameJson {Ru = "Питкерн", En = "Pitcairn"}, Alpha2 = "PN",
        //         Alpha3 = "PCN", Iso = 612, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 166, Name = new TranslationNameJson {Ru = "Польша", En = "Poland"}, Alpha2 = "PL", Alpha3 = "POL",
        //         Iso = 616, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 167, Name = new TranslationNameJson {Ru = "Португалия", En = "Portugal"}, Alpha2 = "PT",
        //         Alpha3 = "PRT", Iso = 620, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 168, Name = new TranslationNameJson {Ru = "Пуэрто-Рико", En = "Puerto Rico"}, Alpha2 = "PR",
        //         Alpha3 = "PRI", Iso = 630, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 169,
        //         Name = new TranslationNameJson
        //             {Ru = "Республика Македония", En = "Macedonia, The Former Yugoslav Republic Of"},
        //         Alpha2 = "MK", Alpha3 = "MKD", Iso = 807, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 170, Name = new TranslationNameJson {Ru = "Реюньон", En = "Reunion"}, Alpha2 = "RE",
        //         Alpha3 = "REU", Iso = 638, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 171, Name = new TranslationNameJson {Ru = "Россия", En = "Russian Federation"}, Alpha2 = "RU",
        //         Alpha3 = "RUS", Iso = 643, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 172, Name = new TranslationNameJson {Ru = "Руанда", En = "Rwanda"}, Alpha2 = "RW", Alpha3 = "RWA",
        //         Iso = 646, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 173, Name = new TranslationNameJson {Ru = "Румыния", En = "Romania"}, Alpha2 = "RO",
        //         Alpha3 = "ROU", Iso = 642, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 174, Name = new TranslationNameJson {Ru = "Самоа", En = "Samoa"}, Alpha2 = "WS", Alpha3 = "WSM",
        //         Iso = 882, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 175, Name = new TranslationNameJson {Ru = "Сан-Марино", En = "San Marino"}, Alpha2 = "SM",
        //         Alpha3 = "SMR", Iso = 674, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 176, Name = new TranslationNameJson {Ru = "Сан-Томе и Принсипи", En = "Sao Tome and Principe"},
        //         Alpha2 = "ST", Alpha3 = "STP", Iso = 678, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 177, Name = new TranslationNameJson {Ru = "Саудовская Аравия", En = "Saudi Arabia"}, Alpha2 = "SA",
        //         Alpha3 = "SAU", Iso = 682, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 178,
        //         Name = new TranslationNameJson
        //         {
        //             Ru = "Святая Елена, Остров вознесения, Тристан-да-Кунья",
        //             En = "Saint Helena, Ascension And Tristan Da Cunha"
        //         },
        //         Alpha2 = "SH", Alpha3 = "SHN", Iso = 654, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 179,
        //         Name = new TranslationNameJson {Ru = "Северные Марианские острова", En = "Northern Mariana Islands"},
        //         Alpha2 = "MP", Alpha3 = "MNP", Iso = 580, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 180, Name = new TranslationNameJson {Ru = "Сен-Бартельми", En = "Saint Barthélemy"}, Alpha2 = "BL",
        //         Alpha3 = "BLM", Iso = 652, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 181, Name = new TranslationNameJson {Ru = "Сен-Мартен", En = "Saint Martin (French Part)"},
        //         Alpha2 = "MF", Alpha3 = "MAF", Iso = 663, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 182, Name = new TranslationNameJson {Ru = "Сенегал", En = "Senegal"}, Alpha2 = "SN",
        //         Alpha3 = "SEN", Iso = 686, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 183,
        //         Name = new TranslationNameJson
        //             {Ru = "Сент-Винсент и Гренадины", En = "Saint Vincent and the Grenadines"},
        //         Alpha2 = "VC", Alpha3 = "VCT", Iso = 670, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 184, Name = new TranslationNameJson {Ru = "Сент-Китс и Невис", En = "Saint Kitts and Nevis"},
        //         Alpha2 = "KN", Alpha3 = "KNA", Iso = 659, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 185, Name = new TranslationNameJson {Ru = "Сент-Люсия", En = "Saint Lucia"}, Alpha2 = "LC",
        //         Alpha3 = "LCA", Iso = 662, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 186, Name = new TranslationNameJson {Ru = "Сент-Пьер и Микелон", En = "Saint Pierre and Miquelon"},
        //         Alpha2 = "PM", Alpha3 = "SPM", Iso = 666, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 187, Name = new TranslationNameJson {Ru = "Сербия", En = "Serbia"}, Alpha2 = "RS", Alpha3 = "SRB",
        //         Iso = 688, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 188, Name = new TranslationNameJson {Ru = "Сейшелы", En = "Seychelles"}, Alpha2 = "SC",
        //         Alpha3 = "SYC", Iso = 690, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 189, Name = new TranslationNameJson {Ru = "Сингапур", En = "Singapore"}, Alpha2 = "SG",
        //         Alpha3 = "SGP", Iso = 702, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 190, Name = new TranslationNameJson {Ru = "Синт-Мартен", En = "Sint Maarten"}, Alpha2 = "SX",
        //         Alpha3 = "SXM", Iso = 534, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 191,
        //         Name = new TranslationNameJson {Ru = "Сирийская Арабская Республика", En = "Syrian Arab Republic"},
        //         Alpha2 = "SY", Alpha3 = "SYR", Iso = 760, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 192, Name = new TranslationNameJson {Ru = "Словакия", En = "Slovakia"}, Alpha2 = "SK",
        //         Alpha3 = "SVK", Iso = 703, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 193, Name = new TranslationNameJson {Ru = "Словения", En = "Slovenia"}, Alpha2 = "SI",
        //         Alpha3 = "SVN", Iso = 705, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 194, Name = new TranslationNameJson {Ru = "Соединенное Королевство", En = "United Kingdom"},
        //         Alpha2 = "GB", Alpha3 = "GBR", Iso = 826, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 195, Name = new TranslationNameJson {Ru = "Соединенные Штаты", En = "United States"},
        //         Alpha2 = "US", Alpha3 = "USA", Iso = 840, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 196, Name = new TranslationNameJson {Ru = "Соломоновы острова", En = "Solomon Islands"},
        //         Alpha2 = "SB", Alpha3 = "SLB", Iso = 90, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 197, Name = new TranslationNameJson {Ru = "Сомали", En = "Somalia"}, Alpha2 = "SO", Alpha3 = "SOM",
        //         Iso = 706, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 198, Name = new TranslationNameJson {Ru = "Судан", En = "Sudan"}, Alpha2 = "SD", Alpha3 = "SDN",
        //         Iso = 729, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 199, Name = new TranslationNameJson {Ru = "Суринам", En = "Suriname"}, Alpha2 = "SR",
        //         Alpha3 = "SUR", Iso = 740, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 200, Name = new TranslationNameJson {Ru = "Сьерра-Леоне", En = "Sierra Leone"}, Alpha2 = "SL",
        //         Alpha3 = "SLE", Iso = 694, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 201, Name = new TranslationNameJson {Ru = "Таджикистан", En = "Tajikistan"}, Alpha2 = "TJ",
        //         Alpha3 = "TJK", Iso = 762, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 202, Name = new TranslationNameJson {Ru = "Таиланд", En = "Thailand"}, Alpha2 = "TH",
        //         Alpha3 = "THA", Iso = 764, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 203, Name = new TranslationNameJson {Ru = "Тайвань (Китай)", En = "Taiwan, Province of China"},
        //         Alpha2 = "TW", Alpha3 = "TWN", Iso = 158, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 204,
        //         Name = new TranslationNameJson
        //             {Ru = "Танзания, Объединенная Республика", En = "Tanzania, United Republic Of"},
        //         Alpha2 = "TZ", Alpha3 = "TZA", Iso = 834, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 205, Name = new TranslationNameJson {Ru = "Тимор-Лесте", En = "Timor-Leste"}, Alpha2 = "TL",
        //         Alpha3 = "TLS", Iso = 626, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 206, Name = new TranslationNameJson {Ru = "Того", En = "Togo"}, Alpha2 = "TG", Alpha3 = "TGO",
        //         Iso = 768, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 207, Name = new TranslationNameJson {Ru = "Токелау", En = "Tokelau"}, Alpha2 = "TK",
        //         Alpha3 = "TKL", Iso = 772, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 208, Name = new TranslationNameJson {Ru = "Тонга", En = "Tonga"}, Alpha2 = "TO", Alpha3 = "TON",
        //         Iso = 776, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 209, Name = new TranslationNameJson {Ru = "Тринидад и Тобаго", En = "Trinidad and Tobago"},
        //         Alpha2 = "TT", Alpha3 = "TTO", Iso = 780, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 210, Name = new TranslationNameJson {Ru = "Тувалу", En = "Tuvalu"}, Alpha2 = "TV", Alpha3 = "TUV",
        //         Iso = 798, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 211, Name = new TranslationNameJson {Ru = "Тунис", En = "Tunisia"}, Alpha2 = "TN", Alpha3 = "TUN",
        //         Iso = 788, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 212, Name = new TranslationNameJson {Ru = "Туркмения", En = "Turkmenistan"}, Alpha2 = "TM",
        //         Alpha3 = "TKM", Iso = 795, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 213, Name = new TranslationNameJson {Ru = "Турция", En = "Turkey"}, Alpha2 = "TR", Alpha3 = "TUR",
        //         Iso = 792, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 214, Name = new TranslationNameJson {Ru = "Уганда", En = "Uganda"}, Alpha2 = "UG", Alpha3 = "UGA",
        //         Iso = 800, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 215, Name = new TranslationNameJson {Ru = "Узбекистан", En = "Uzbekistan"}, Alpha2 = "UZ",
        //         Alpha3 = "UZB", Iso = 860, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 216, Name = new TranslationNameJson {Ru = "Украина", En = "Ukraine"}, Alpha2 = "UA",
        //         Alpha3 = "UKR", Iso = 804, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 217, Name = new TranslationNameJson {Ru = "Уоллис и Футуна", En = "Wallis and Futuna"},
        //         Alpha2 = "WF", Alpha3 = "WLF", Iso = 876, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 218, Name = new TranslationNameJson {Ru = "Уругвай", En = "Uruguay"}, Alpha2 = "UY",
        //         Alpha3 = "URY", Iso = 858, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 219, Name = new TranslationNameJson {Ru = "Фарерские острова", En = "Faroe Islands"},
        //         Alpha2 = "FO", Alpha3 = "FRO", Iso = 234, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 220, Name = new TranslationNameJson {Ru = "Фиджи", En = "Fiji"}, Alpha2 = "FJ", Alpha3 = "FJI",
        //         Iso = 242, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 221, Name = new TranslationNameJson {Ru = "Филиппины", En = "Philippines"}, Alpha2 = "PH",
        //         Alpha3 = "PHL", Iso = 608, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 222, Name = new TranslationNameJson {Ru = "Финляндия", En = "Finland"}, Alpha2 = "FI",
        //         Alpha3 = "FIN", Iso = 246, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 223,
        //         Name = new TranslationNameJson
        //             {Ru = "Фолклендские острова (Мальвинские)", En = "Falkland Islands (Malvinas)"},
        //         Alpha2 = "FK", Alpha3 = "FLK", Iso = 238, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 224, Name = new TranslationNameJson {Ru = "Франция", En = "France"}, Alpha2 = "FR", Alpha3 = "FRA",
        //         Iso = 250, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 225, Name = new TranslationNameJson {Ru = "Французская Гвиана", En = "French Guiana"},
        //         Alpha2 = "GF", Alpha3 = "GUF", Iso = 254, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 226, Name = new TranslationNameJson {Ru = "Французская Полинезия", En = "French Polynesia"},
        //         Alpha2 = "PF", Alpha3 = "PYF", Iso = 258, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 227,
        //         Name = new TranslationNameJson
        //             {Ru = "Французские Южные территории", En = "French Southern Territories"},
        //         Alpha2 = "TF", Alpha3 = "ATF", Iso = 260, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 228, Name = new TranslationNameJson {Ru = "Хорватия", En = "Croatia"}, Alpha2 = "HR",
        //         Alpha3 = "HRV", Iso = 191, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 229,
        //         Name = new TranslationNameJson
        //             {Ru = "Центрально-Африканская Республика", En = "Central African Republic"},
        //         Alpha2 = "CF", Alpha3 = "CAF", Iso = 140, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 230, Name = new TranslationNameJson {Ru = "Чад", En = "Chad"}, Alpha2 = "TD", Alpha3 = "TCD",
        //         Iso = 148, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 231, Name = new TranslationNameJson {Ru = "Черногория", En = "Montenegro"}, Alpha2 = "ME",
        //         Alpha3 = "MNE", Iso = 499, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 232, Name = new TranslationNameJson {Ru = "Чешская Республика", En = "Czech Republic"},
        //         Alpha2 = "CZ", Alpha3 = "CZE", Iso = 203, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 233, Name = new TranslationNameJson {Ru = "Чили", En = "Chile"}, Alpha2 = "CL", Alpha3 = "CHL",
        //         Iso = 152, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 234, Name = new TranslationNameJson {Ru = "Швейцария", En = "Switzerland"}, Alpha2 = "CH",
        //         Alpha3 = "CHE", Iso = 756, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 235, Name = new TranslationNameJson {Ru = "Швеция", En = "Sweden"}, Alpha2 = "SE", Alpha3 = "SWE",
        //         Iso = 752, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 236, Name = new TranslationNameJson {Ru = "Шпицберген и Ян Майен", En = "Svalbard and Jan Mayen"},
        //         Alpha2 = "SJ", Alpha3 = "SJM", Iso = 744, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 237, Name = new TranslationNameJson {Ru = "Шри-Ланка", En = "Sri Lanka"}, Alpha2 = "LK",
        //         Alpha3 = "LKA", Iso = 144, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 238, Name = new TranslationNameJson {Ru = "Эквадор", En = "Ecuador"}, Alpha2 = "EC",
        //         Alpha3 = "ECU", Iso = 218, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 239, Name = new TranslationNameJson {Ru = "Экваториальная Гвинея", En = "Equatorial Guinea"},
        //         Alpha2 = "GQ", Alpha3 = "GNQ", Iso = 226, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 240, Name = new TranslationNameJson {Ru = "Эландские острова", En = "Åland Islands"},
        //         Alpha2 = "AX", Alpha3 = "ALA", Iso = 248, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 241, Name = new TranslationNameJson {Ru = "Эль-Сальвадор", En = "El Salvador"}, Alpha2 = "SV",
        //         Alpha3 = "SLV", Iso = 222, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 242, Name = new TranslationNameJson {Ru = "Эритрея", En = "Eritrea"}, Alpha2 = "ER",
        //         Alpha3 = "ERI", Iso = 232, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 243, Name = new TranslationNameJson {Ru = "Эсватини", En = "Eswatini"}, Alpha2 = "SZ",
        //         Alpha3 = "SWZ", Iso = 748, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 244, Name = new TranslationNameJson {Ru = "Эстония", En = "Estonia"}, Alpha2 = "EE",
        //         Alpha3 = "EST", Iso = 233, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 245, Name = new TranslationNameJson {Ru = "Эфиопия", En = "Ethiopia"}, Alpha2 = "ET",
        //         Alpha3 = "ETH", Iso = 231, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 246, Name = new TranslationNameJson {Ru = "Южная Африка", En = "South Africa"}, Alpha2 = "ZA",
        //         Alpha3 = "ZAF", Iso = 710, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 247,
        //         Name = new TranslationNameJson
        //         {
        //             Ru = "Южная Джорджия и Южные Сандвичевы острова",
        //             En = "South Georgia and the South Sandwich Islands"
        //         },
        //         Alpha2 = "GS", Alpha3 = "SGS", Iso = 239, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 248, Name = new TranslationNameJson {Ru = "Южная Осетия", En = "South Ossetia"}, Alpha2 = "OS",
        //         Alpha3 = "OST", Iso = 896, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 249, Name = new TranslationNameJson {Ru = "Южный Судан", En = "South Sudan"}, Alpha2 = "SS",
        //         Alpha3 = "SSD", Iso = 728, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 250, Name = new TranslationNameJson {Ru = "Ямайка", En = "Jamaica"}, Alpha2 = "JM", Alpha3 = "JAM",
        //         Iso = 388, CreatedAt = dt, UpdatedAt = dt
        //     },
        //     new Country
        //     {
        //         Id = 251, Name = new TranslationNameJson {Ru = "Япония", En = "Japan"}, Alpha2 = "JP", Alpha3 = "JPN",
        //         Iso = 392, CreatedAt = dt, UpdatedAt = dt
        //     }
        // );
    }
}