﻿using Entities.Auth;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Utils.Enums;

namespace DataAccess.PostgreSql.Configs;

public class UserRoleConfig: IEntityTypeConfiguration<UserRole> 
{
    public void Configure(EntityTypeBuilder<UserRole> builder)
    {
        builder.HasData(
            new UserRole {RoleId = RoleEnum.Admin, UserId = 1},
            new UserRole {RoleId = RoleEnum.Traceca, UserId = 1}
        );
    }
}