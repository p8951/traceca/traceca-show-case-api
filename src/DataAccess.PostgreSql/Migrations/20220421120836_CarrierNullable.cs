﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class CarrierNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "city",
                table: "carriers",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "address",
                table: "carriers",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<string>(
                name: "additional_field_values",
                table: "carriers",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8493), new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8496) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8497), new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8497) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 954, DateTimeKind.Utc).AddTicks(5399), new DateTime(2022, 4, 21, 12, 8, 35, 954, DateTimeKind.Utc).AddTicks(5401) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "city",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "address",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "additional_field_values",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9940), new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9943) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9945), new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9945) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 527, DateTimeKind.Utc).AddTicks(5840), new DateTime(2022, 4, 21, 9, 56, 4, 527, DateTimeKind.Utc).AddTicks(5842) });
        }
    }
}
