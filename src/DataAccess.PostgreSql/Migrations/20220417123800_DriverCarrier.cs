﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class DriverCarrier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "user_roles",
                keyColumns: new[] { "role_id", "user_id" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "roles",
                keyColumn: "id",
                keyValue: 2);

            migrationBuilder.CreateTable(
                name: "driver_carriers",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    carrier_id = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_driver_carriers", x => x.id);
                    table.ForeignKey(
                        name: "fk_driver_carriers_carriers_carrier_id",
                        column: x => x.carrier_id,
                        principalTable: "carriers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_driver_carriers_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1577), new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1579) });

            migrationBuilder.InsertData(
                table: "roles",
                columns: new[] { "id", "code", "created_at", "name", "updated_at" },
                values: new object[] { 5, "user", new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1580), "User", new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1580) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 17, 12, 38, 0, 91, DateTimeKind.Utc).AddTicks(836), new DateTime(2022, 4, 17, 12, 38, 0, 91, DateTimeKind.Utc).AddTicks(837) });

            migrationBuilder.InsertData(
                table: "user_roles",
                columns: new[] { "role_id", "user_id" },
                values: new object[] { 5, 1 });

            migrationBuilder.CreateIndex(
                name: "ix_driver_carriers_carrier_id",
                table: "driver_carriers",
                column: "carrier_id");

            migrationBuilder.CreateIndex(
                name: "ix_driver_carriers_user_id",
                table: "driver_carriers",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "driver_carriers");

            migrationBuilder.DeleteData(
                table: "user_roles",
                keyColumns: new[] { "role_id", "user_id" },
                keyValues: new object[] { 5, 1 });

            migrationBuilder.DeleteData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2171), new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2174) });

            migrationBuilder.InsertData(
                table: "roles",
                columns: new[] { "id", "code", "created_at", "name", "updated_at" },
                values: new object[] { 2, "user", new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2176), "User", new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2176) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 16, 18, 48, 31, DateTimeKind.Utc).AddTicks(789), new DateTime(2022, 4, 10, 16, 18, 48, 31, DateTimeKind.Utc).AddTicks(790) });

            migrationBuilder.InsertData(
                table: "user_roles",
                columns: new[] { "role_id", "user_id" },
                values: new object[] { 2, 1 });
        }
    }
}
