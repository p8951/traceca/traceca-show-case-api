﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class TransportUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "user_id",
                table: "transports",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7827), new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7829) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7830), new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7831) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 36, DateTimeKind.Utc).AddTicks(6575), new DateTime(2022, 4, 18, 9, 54, 59, 36, DateTimeKind.Utc).AddTicks(6576) });

            migrationBuilder.CreateIndex(
                name: "ix_transports_user_id",
                table: "transports",
                column: "user_id");

            migrationBuilder.AddForeignKey(
                name: "fk_transports_users_user_id",
                table: "transports",
                column: "user_id",
                principalTable: "users",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_transports_users_user_id",
                table: "transports");

            migrationBuilder.DropIndex(
                name: "ix_transports_user_id",
                table: "transports");

            migrationBuilder.DropColumn(
                name: "user_id",
                table: "transports");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1577), new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1579) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1580), new DateTime(2022, 4, 17, 12, 38, 0, 89, DateTimeKind.Utc).AddTicks(1580) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 17, 12, 38, 0, 91, DateTimeKind.Utc).AddTicks(836), new DateTime(2022, 4, 17, 12, 38, 0, 91, DateTimeKind.Utc).AddTicks(837) });
        }
    }
}
