﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class CarrierAdderssFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "city",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "email",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5881), new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5883) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5885), new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5885) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 63, DateTimeKind.Utc).AddTicks(3140), new DateTime(2022, 4, 10, 13, 45, 39, 63, DateTimeKind.Utc).AddTicks(3141) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "carriers");

            migrationBuilder.DropColumn(
                name: "city",
                table: "carriers");

            migrationBuilder.DropColumn(
                name: "email",
                table: "carriers");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9712), new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9714) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9715), new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9716) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 3, 20, 10, 18, 19, 717, DateTimeKind.Utc).AddTicks(7101), new DateTime(2022, 3, 20, 10, 18, 19, 717, DateTimeKind.Utc).AddTicks(7102) });
        }
    }
}
