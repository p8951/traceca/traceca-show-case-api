﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class TransportAdditionalNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "additional_field_values",
                table: "transports",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1573), new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1575) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1577), new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1577) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 750, DateTimeKind.Utc).AddTicks(755), new DateTime(2022, 4, 18, 12, 4, 51, 750, DateTimeKind.Utc).AddTicks(756) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "additional_field_values",
                table: "transports",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7827), new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7829) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7830), new DateTime(2022, 4, 18, 9, 54, 59, 34, DateTimeKind.Utc).AddTicks(7831) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 9, 54, 59, 36, DateTimeKind.Utc).AddTicks(6575), new DateTime(2022, 4, 18, 9, 54, 59, 36, DateTimeKind.Utc).AddTicks(6576) });
        }
    }
}
