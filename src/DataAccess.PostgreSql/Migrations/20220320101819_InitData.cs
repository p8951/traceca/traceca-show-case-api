﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Utils;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class InitData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "countries",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'7', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<TranslationNameJson>(type: "jsonb", nullable: false),
                    alpha2 = table.Column<string>(type: "text", nullable: false),
                    alpha3 = table.Column<string>(type: "text", nullable: false),
                    iso = table.Column<short>(type: "smallint", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_countries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    code = table.Column<string>(type: "text", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_roles", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "carriers",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: false),
                    country_id = table.Column<int>(type: "integer", nullable: false),
                    contacts = table.Column<string>(type: "text", nullable: false),
                    organization = table.Column<string>(type: "text", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_carriers", x => x.id);
                    table.ForeignKey(
                        name: "fk_carriers_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "country_couple_year_permit_counts",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    first_country_id = table.Column<int>(type: "integer", nullable: false),
                    second_country_id = table.Column<int>(type: "integer", nullable: false),
                    year = table.Column<short>(type: "smallint", nullable: false),
                    count_permit = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_country_couple_year_permit_counts", x => x.id);
                    table.ForeignKey(
                        name: "fk_country_couple_year_permit_counts_countries_first_country_id",
                        column: x => x.first_country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_country_couple_year_permit_counts_countries_second_country_",
                        column: x => x.second_country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "user_registrations",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    last_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    first_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    patronymic = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    email = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    activated = table.Column<bool>(type: "boolean", nullable: false),
                    activated_date_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    role = table.Column<int>(type: "integer", nullable: false),
                    country_id = table.Column<int>(type: "integer", nullable: false),
                    organization = table.Column<string>(type: "text", nullable: false),
                    contacts = table.Column<string>(type: "text", nullable: false),
                    password = table.Column<string>(type: "text", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_registrations", x => x.id);
                    table.ForeignKey(
                        name: "fk_user_registrations_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:IdentitySequenceOptions", "'2', '1', '', '', 'False', '1'")
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    last_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    first_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    patronymic = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    password_hash = table.Column<string>(type: "text", nullable: false),
                    username = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    email = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false),
                    is_active = table.Column<bool>(type: "boolean", nullable: false),
                    country_id = table.Column<int>(type: "integer", nullable: false),
                    organization = table.Column<string>(type: "text", nullable: false),
                    contacts = table.Column<string>(type: "text", nullable: false),
                    carrier_id = table.Column<int>(type: "integer", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_users", x => x.id);
                    table.ForeignKey(
                        name: "fk_users_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "transports",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    carrier_id = table.Column<int>(type: "integer", nullable: false),
                    government_number = table.Column<string>(type: "text", nullable: false),
                    driver = table.Column<string>(type: "text", nullable: false),
                    model = table.Column<string>(type: "text", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transports", x => x.id);
                    table.ForeignKey(
                        name: "fk_transports_carriers_carrier_id",
                        column: x => x.carrier_id,
                        principalTable: "carriers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "additional_permissions",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    country_couple_year_permit_count_id = table.Column<int>(type: "integer", nullable: false),
                    fields = table.Column<string>(type: "text", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_additional_permissions", x => x.id);
                    table.ForeignKey(
                        name: "fk_additional_permissions_country_couple_year_permit_counts_co",
                        column: x => x.country_couple_year_permit_count_id,
                        principalTable: "country_couple_year_permit_counts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "country_couple_permit_counts",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    country_couple_year_permit_count_id = table.Column<int>(type: "integer", nullable: false),
                    permit_count = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_country_couple_permit_counts", x => x.id);
                    table.ForeignKey(
                        name: "fk_country_couple_permit_counts_country_couple_year_permit_cou",
                        column: x => x.country_couple_year_permit_count_id,
                        principalTable: "country_couple_year_permit_counts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "permits",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    country_couple_year_permit_count_id = table.Column<int>(type: "integer", nullable: false),
                    permit_code = table.Column<string>(type: "text", nullable: false),
                    used = table.Column<bool>(type: "boolean", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permits", x => x.id);
                    table.ForeignKey(
                        name: "fk_permits_country_couple_year_permit_counts_country_couple_ye",
                        column: x => x.country_couple_year_permit_count_id,
                        principalTable: "country_couple_year_permit_counts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transit_countries",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    country_couple_year_permit_count_id = table.Column<int>(type: "integer", nullable: false),
                    country_id = table.Column<int>(type: "integer", nullable: false),
                    country_couple_year_permit_count_id1 = table.Column<int>(type: "integer", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transit_countries", x => x.id);
                    table.ForeignKey(
                        name: "fk_transit_countries_countries_country_id",
                        column: x => x.country_id,
                        principalTable: "countries",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_transit_countries_country_couple_year_permit_counts_country",
                        column: x => x.country_couple_year_permit_count_id,
                        principalTable: "country_couple_year_permit_counts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_transit_countries_country_couple_year_permit_counts_country1",
                        column: x => x.country_couple_year_permit_count_id1,
                        principalTable: "country_couple_year_permit_counts",
                        principalColumn: "id");
                });

            migrationBuilder.CreateTable(
                name: "refresh_token",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    token = table.Column<string>(type: "text", nullable: false),
                    expires = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    created_by_ip = table.Column<string>(type: "text", nullable: false),
                    revoked = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    revoked_by_ip = table.Column<string>(type: "text", nullable: true),
                    replaced_by_token = table.Column<string>(type: "text", nullable: true),
                    reason_revoked = table.Column<string>(type: "text", nullable: true),
                    user_id = table.Column<int>(type: "integer", nullable: true),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_refresh_token", x => x.id);
                    table.ForeignKey(
                        name: "fk_refresh_token_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "user_roles",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    role_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_roles", x => new { x.role_id, x.user_id });
                    table.ForeignKey(
                        name: "fk_user_roles_roles_role_id",
                        column: x => x.role_id,
                        principalTable: "roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_user_roles_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "carrier_permit_counts",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    carrier_id = table.Column<int>(type: "integer", nullable: false),
                    country_couple_permit_count_id = table.Column<int>(type: "integer", nullable: false),
                    permit_count = table.Column<int>(type: "integer", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_carrier_permit_counts", x => x.id);
                    table.ForeignKey(
                        name: "fk_carrier_permit_counts_carriers_carrier_id",
                        column: x => x.carrier_id,
                        principalTable: "carriers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_carrier_permit_counts_country_couple_permit_counts_country_",
                        column: x => x.country_couple_permit_count_id,
                        principalTable: "country_couple_permit_counts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "transport_permits",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    transport_id = table.Column<int>(type: "integer", nullable: false),
                    permit_id = table.Column<long>(type: "bigint", nullable: false),
                    used = table.Column<bool>(type: "boolean", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    updated_at = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_transport_permits", x => x.id);
                    table.ForeignKey(
                        name: "fk_transport_permits_permits_permit_id",
                        column: x => x.permit_id,
                        principalTable: "permits",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_transport_permits_transports_transport_id",
                        column: x => x.transport_id,
                        principalTable: "transports",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

             migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 1, "{\"ru\":\"Армения\", \"en\": \"Armenia\"}", "AM", "ARM", 51, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 2, "{\"ru\":\"Болгария\", \"en\": \"Bulgaria\"}", "BG", "BGR", 100, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 3, "{\"ru\":\"Грузия\", \"en\": \"Georgia\"}", "GE", "GEO", 268, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 4, "{\"ru\":\"Азербайджан\", \"en\": \"Azerbaijan\"}", "AZ", "AZE", 31, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 5, "{\"ru\":\"Иран\", \"en\": \"Iran\"}", "IR", "IRN", 364, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});
            migrationBuilder.InsertData(
                table: "countries",
                columns: new[] { "id", "name", "alpha2", "alpha3", "iso", "created_at", "updated_at" },
                values: new object[] { 6, "{\"ru\":\"Казахстан\", \"en\": \"Kazakhstan\"}", "KZ", "KAZ", 398, new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8940), new DateTime(2022, 3, 7, 9, 39, 25, 343, DateTimeKind.Utc).AddTicks(8941)});

            
            migrationBuilder.InsertData(
                table: "roles",
                columns: new[] { "id", "code", "created_at", "name", "updated_at" },
                values: new object[,]
                {
                    { 1, "admin", new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9712), "Administrator", new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9714) },
                    { 2, "user", new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9715), "User", new DateTime(2022, 3, 20, 10, 18, 19, 715, DateTimeKind.Utc).AddTicks(9716) }
                });

            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "id", "carrier_id", "contacts", "country_id", "created_at", "email", "first_name", "is_active", "last_name", "organization", "password_hash", "patronymic", "updated_at", "username" },
                values: new object[] { 1, null, "", 1, new DateTime(2022, 3, 20, 10, 18, 19, 717, DateTimeKind.Utc).AddTicks(7101), "e.arbabayev@kmg.kz", "Иван", true, "Иванов", "", "$2a$11$i0cRGfSNEq.ezit.c.hoE.FcYNhP15d6PNjVZfQ4OQKySTkr08Y5G", "Иванович", new DateTime(2022, 3, 20, 10, 18, 19, 717, DateTimeKind.Utc).AddTicks(7102), "admin" });

            migrationBuilder.InsertData(
                table: "user_roles",
                columns: new[] { "role_id", "user_id" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 }
                });

            
            
            
           
            
            
            
            migrationBuilder.CreateIndex(
                name: "ix_additional_permissions_country_couple_year_permit_count_id",
                table: "additional_permissions",
                column: "country_couple_year_permit_count_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_carrier_permit_counts_carrier_id",
                table: "carrier_permit_counts",
                column: "carrier_id");

            migrationBuilder.CreateIndex(
                name: "ix_carrier_permit_counts_country_couple_permit_count_id",
                table: "carrier_permit_counts",
                column: "country_couple_permit_count_id");

            migrationBuilder.CreateIndex(
                name: "ix_carriers_country_id",
                table: "carriers",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_country_couple_permit_counts_country_couple_year_permit_cou",
                table: "country_couple_permit_counts",
                column: "country_couple_year_permit_count_id");

            migrationBuilder.CreateIndex(
                name: "ix_country_couple_year_permit_counts_first_country_id",
                table: "country_couple_year_permit_counts",
                column: "first_country_id");

            migrationBuilder.CreateIndex(
                name: "ix_country_couple_year_permit_counts_second_country_id",
                table: "country_couple_year_permit_counts",
                column: "second_country_id");

            migrationBuilder.CreateIndex(
                name: "ix_permits_country_couple_year_permit_count_id",
                table: "permits",
                column: "country_couple_year_permit_count_id");

            migrationBuilder.CreateIndex(
                name: "ix_refresh_token_user_id",
                table: "refresh_token",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_roles_code",
                table: "roles",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_transit_countries_country_couple_year_permit_count_id",
                table: "transit_countries",
                column: "country_couple_year_permit_count_id");

            migrationBuilder.CreateIndex(
                name: "ix_transit_countries_country_couple_year_permit_count_id1",
                table: "transit_countries",
                column: "country_couple_year_permit_count_id1");

            migrationBuilder.CreateIndex(
                name: "ix_transit_countries_country_id",
                table: "transit_countries",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_transport_permits_permit_id",
                table: "transport_permits",
                column: "permit_id");

            migrationBuilder.CreateIndex(
                name: "ix_transport_permits_transport_id",
                table: "transport_permits",
                column: "transport_id");

            migrationBuilder.CreateIndex(
                name: "ix_transports_carrier_id",
                table: "transports",
                column: "carrier_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_registrations_country_id",
                table: "user_registrations",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_user_roles_user_id",
                table: "user_roles",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "ix_users_country_id",
                table: "users",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "ix_users_username",
                table: "users",
                column: "username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "additional_permissions");

            migrationBuilder.DropTable(
                name: "carrier_permit_counts");

            migrationBuilder.DropTable(
                name: "refresh_token");

            migrationBuilder.DropTable(
                name: "transit_countries");

            migrationBuilder.DropTable(
                name: "transport_permits");

            migrationBuilder.DropTable(
                name: "user_registrations");

            migrationBuilder.DropTable(
                name: "user_roles");

            migrationBuilder.DropTable(
                name: "country_couple_permit_counts");

            migrationBuilder.DropTable(
                name: "permits");

            migrationBuilder.DropTable(
                name: "transports");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "country_couple_year_permit_counts");

            migrationBuilder.DropTable(
                name: "carriers");

            migrationBuilder.DropTable(
                name: "countries");
        }
    }
}
