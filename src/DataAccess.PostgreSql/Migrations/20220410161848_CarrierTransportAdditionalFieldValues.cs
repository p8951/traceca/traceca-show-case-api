﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class CarrierTransportAdditionalFieldValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "additional_field_values",
                table: "transports",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "additional_field_values",
                table: "carriers",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2171), new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2174) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2176), new DateTime(2022, 4, 10, 16, 18, 48, 29, DateTimeKind.Utc).AddTicks(2176) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 16, 18, 48, 31, DateTimeKind.Utc).AddTicks(789), new DateTime(2022, 4, 10, 16, 18, 48, 31, DateTimeKind.Utc).AddTicks(790) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "additional_field_values",
                table: "transports");

            migrationBuilder.DropColumn(
                name: "additional_field_values",
                table: "carriers");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5881), new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5883) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5885), new DateTime(2022, 4, 10, 13, 45, 39, 61, DateTimeKind.Utc).AddTicks(5885) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 10, 13, 45, 39, 63, DateTimeKind.Utc).AddTicks(3140), new DateTime(2022, 4, 10, 13, 45, 39, 63, DateTimeKind.Utc).AddTicks(3141) });
        }
    }
}
