﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class UserRegisterAddressCity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "organization",
                table: "user_registrations",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "user_registrations",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "city",
                table: "user_registrations",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 25, 9, 15, 56, 423, DateTimeKind.Utc).AddTicks(670), new DateTime(2022, 4, 25, 9, 15, 56, 423, DateTimeKind.Utc).AddTicks(673) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 25, 9, 15, 56, 423, DateTimeKind.Utc).AddTicks(675), new DateTime(2022, 4, 25, 9, 15, 56, 423, DateTimeKind.Utc).AddTicks(675) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 25, 9, 15, 56, 424, DateTimeKind.Utc).AddTicks(9853), new DateTime(2022, 4, 25, 9, 15, 56, 424, DateTimeKind.Utc).AddTicks(9855) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "user_registrations");

            migrationBuilder.DropColumn(
                name: "city",
                table: "user_registrations");

            migrationBuilder.AlterColumn<string>(
                name: "organization",
                table: "user_registrations",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8493), new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8496) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8497), new DateTime(2022, 4, 21, 12, 8, 35, 952, DateTimeKind.Utc).AddTicks(8497) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 12, 8, 35, 954, DateTimeKind.Utc).AddTicks(5399), new DateTime(2022, 4, 21, 12, 8, 35, 954, DateTimeKind.Utc).AddTicks(5401) });
        }
    }
}
