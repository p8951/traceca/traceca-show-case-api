﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.PostgreSql.Migrations
{
    public partial class RemoveReference : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_transit_countries_country_couple_year_permit_counts_country1",
                table: "transit_countries");

            migrationBuilder.DropIndex(
                name: "ix_transit_countries_country_couple_year_permit_count_id1",
                table: "transit_countries");

            migrationBuilder.DropColumn(
                name: "country_couple_year_permit_count_id1",
                table: "transit_countries");

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9940), new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9943) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9945), new DateTime(2022, 4, 21, 9, 56, 4, 525, DateTimeKind.Utc).AddTicks(9945) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 21, 9, 56, 4, 527, DateTimeKind.Utc).AddTicks(5840), new DateTime(2022, 4, 21, 9, 56, 4, 527, DateTimeKind.Utc).AddTicks(5842) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "country_couple_year_permit_count_id1",
                table: "transit_countries",
                type: "integer",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1573), new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1575) });

            migrationBuilder.UpdateData(
                table: "roles",
                keyColumn: "id",
                keyValue: 5,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1577), new DateTime(2022, 4, 18, 12, 4, 51, 748, DateTimeKind.Utc).AddTicks(1577) });

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_at", "updated_at" },
                values: new object[] { new DateTime(2022, 4, 18, 12, 4, 51, 750, DateTimeKind.Utc).AddTicks(755), new DateTime(2022, 4, 18, 12, 4, 51, 750, DateTimeKind.Utc).AddTicks(756) });

            migrationBuilder.CreateIndex(
                name: "ix_transit_countries_country_couple_year_permit_count_id1",
                table: "transit_countries",
                column: "country_couple_year_permit_count_id1");

            migrationBuilder.AddForeignKey(
                name: "fk_transit_countries_country_couple_year_permit_counts_country1",
                table: "transit_countries",
                column: "country_couple_year_permit_count_id1",
                principalTable: "country_couple_year_permit_counts",
                principalColumn: "id");
        }
    }
}
