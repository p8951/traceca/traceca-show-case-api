using Auth.Interfaces;
using Auth.Interfaces.Dto;

namespace Auth.Jwt;

using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

public class JwtUtils : IJwtUtils
{
    private readonly JwtAppSettings _jwtAppSettings;

    public JwtUtils(IOptions<JwtAppSettings> appSettings)
    {
        _jwtAppSettings = appSettings.Value;
    }

    public string GenerateJwtToken(AuthUserDto user)
    {
        // generate token that is valid for 15 minutes
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_jwtAppSettings.Secret);

        var roleClaims = user.Roles.Select(t => new Claim(ClaimTypes.Role, t.Id.ToString())).ToList();


        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("id", user.Id.ToString())
            }.Union(roleClaims)),
            Expires = DateTime.UtcNow.AddMinutes(60),
            SigningCredentials =
                new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            Audience = _jwtAppSettings.Audience,
            Issuer = _jwtAppSettings.Issuer
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }

    public int? ValidateJwtToken(string token)
    {
        if (token == null)
            return null;

        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_jwtAppSettings.Secret);
        try
        {
            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken) validatedToken;
            var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

            // return user id from JWT token if validation successful
            return userId;
        }
        catch
        {
            // return null if validation fails
            return null;
        }
    }

    public AuthRefreshTokenDto GenerateRefreshToken(string ipAddress)
    {
        var refreshToken = new AuthRefreshTokenDto
        {
            Token = GetUniqueToken(),
            Expires = DateTime.UtcNow.AddDays(1),
            CreatedAt = DateTime.UtcNow,
            CreatedByIp = ipAddress
        };

        return refreshToken;

        string GetUniqueToken()
        {
            // token is a cryptographically strong random sequence of values
            return Convert.ToBase64String(RandomNumberGenerator.GetBytes(64));
        }
    }
}