﻿namespace Utils.Enums;

public enum RoleEnum
{
    Admin = 1,
    Country = 2,
    Carrier = 3,
    Transport = 4,
    Traceca = 5
}