﻿namespace Utils.Enums;

public enum PermitStatusEnum
{
    /// <summary>
    /// Разрешение создано
    /// </summary>
    Created = 0,
    /// <summary>
    /// Доступность разшенеия второй стране в паре
    /// </summary>
    Activated  = 1,
    /// <summary>
    /// Разрешение использовано
    /// </summary>
    Used  = 2
}