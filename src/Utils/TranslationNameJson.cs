﻿using System.Text.Json.Serialization;

namespace Utils;

public class TranslationNameJson
{
    [JsonPropertyName("ru")]
    public string Ru { get; set; }
    [JsonPropertyName("en")]
    public string En { get; set; }
}