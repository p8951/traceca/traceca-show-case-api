﻿using Utils.Enums;

namespace Utils.Json;

public class AdditionalFieldJson
{
    public string FieldName { get; set; }
    public bool IsActive { get; set; } = true;
    public RoleEnum Role { get; set; }
}