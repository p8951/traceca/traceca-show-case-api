﻿namespace Utils.Json;

public class AdditionalFieldValueJson
{
    public string FieldName { get; set; }
    public string FieldValue { get; set; }
    public bool IsActive { get; set; } = true;
}